using SGEnrollment_Windows_Form1 = SGEnrollment.Windows_Form1;

using SGEnrollment__GC = SGEnrollment._GC;

using SGEnrollment_Amisys = SGEnrollment.Amisys;

using SGEnrollment_SGE_P_BR0200_BrokerInfo = SGEnrollment.SGE_P_BR0200_BrokerInfo;

using SGEnrollment_SGE_P_IR0600_1095Info = SGEnrollment.SGE_P_IR0600_1095Info;

using SGEnrollment_SGE_P_ME0210_GroupBilling = SGEnrollment.SGE_P_ME0210_GroupBilling;

using SGEnrollment_SGE_P_ME0300_DivisionSetup = SGEnrollment.SGE_P_ME0300_DivisionSetup;

using SGEnrollment_SGE_P_RF0600_Address = SGEnrollment.SGE_P_RF0600_Address;

using SGEnrollment_SGE_P_RM0100_Remarks = SGEnrollment.SGE_P_RM0100_Remarks;

using SGEnrollment_SGE_A_BotActivity = SGEnrollment.SGE_A_BotActivity;

using SGEnrollment_SGE_P_ME0200_NewGroupSetup = SGEnrollment.SGE_P_ME0200_NewGroupSetup;

using SGEnrollment_SGE_E_BotStart = SGEnrollment.SGE_E_BotStart;

using SGEnrollment_SGE_E_BotStop = SGEnrollment.SGE_E_BotStop;

using SGEnrollment_SGE_P_AllCloseWindows = SGEnrollment.SGE_P_AllCloseWindows;

using SGEnrollment_SGE_P_AppLogin = SGEnrollment.SGE_P_AppLogin;

using SGEnrollment_SGE_P_DivisionSetup_Loop = SGEnrollment.SGE_P_DivisionSetup_Loop;

using SGEnrollment_SGE_Main = SGEnrollment.SGE_Main;

using System;

namespace SGEnrollment.Project
{
// Project-8D892BE3F78321E
/// <summary>
/// OpenSpan project.
/// </summary>
[OpenSpan.Design.TargetEnvironmentAttribute(OpenSpan.Design.TargetEnvironment.Driver)]
[OpenSpan.Design.ComponentIdentityAttribute("Project-8D892BE3F78321E")]
[OpenSpan.Design.DeploymentVersionAttribute("1.0")]
[OpenSpan.Design.ProjectDescriptorAttribute(OpenSpan.Design.ProjectDescriptorType.Present)]
[OpenSpan.Design.ProjectDescriptorAttribute(OpenSpan.Design.ProjectDescriptorType.ContainsAdapters)]
public sealed class SGEnrollment : OpenSpan.Runtime.RuntimeProject
{
	
	public SGEnrollment() : 
			base()
	{
		// 
		// Global Initialization
		// 
		OpenSpan.Diagnostics.Diagnostic.Initialize();
		// 
		// Initialize properties
		// 
		this.SGEnrollment_Initialize();
		// 
		// Initialize Runtime
		// 
		base.Initialize();
	}
	
	public SGEnrollment(System.ComponentModel.ISynchronizeInvoke syncObject) : 
			base(syncObject)
	{
		// 
		// Global Initialization
		// 
		OpenSpan.Diagnostics.Diagnostic.Initialize();
		// 
		// Initialize properties
		// 
		this.SGEnrollment_Initialize();
		// 
		// Initialize Runtime
		// 
		base.Initialize();
	}
	
	public SGEnrollment(System.IServiceProvider serviceProvider) : 
			base(serviceProvider)
	{
		// 
		// Global Initialization
		// 
		OpenSpan.Diagnostics.Diagnostic.Initialize();
		// 
		// Initialize properties
		// 
		this.SGEnrollment_Initialize();
		// 
		// Initialize Runtime
		// 
		base.Initialize();
	}
	
	public SGEnrollment(bool initializeScout) : 
			base(initializeScout)
	{
		// 
		// Global Initialization
		// 
		OpenSpan.Diagnostics.Diagnostic.Initialize();
		// 
		// Initialize properties
		// 
		this.SGEnrollment_Initialize();
		// 
		// Initialize Runtime
		// 
		base.Initialize();
	}
	
	private SGEnrollment(System.IServiceProvider serviceProvider, OpenSpan.Runtime.RuntimeHostCommandLineHelper commandLineHelper, OpenSpan.Deployment.DeploymentManifest deploymentManifest) : 
			base(serviceProvider, commandLineHelper, deploymentManifest)
	{
		// 
		// Global Initialization
		// 
		OpenSpan.Diagnostics.Diagnostic.Initialize();
		// 
		// Initialize properties
		// 
		this.SGEnrollment_Initialize();
		// 
		// Initialize Runtime
		// 
		base.Initialize();
	}
	
	private void SGEnrollment_Initialize()
	{
		this.mId = new OpenSpan.Design.ComponentIdentity("Project-8D892BE3F78321E");
		this.mVersion = new System.Version("19.1.69.0");
		this.mDeploymentVersion = "1.0";
		this.mTransformationVersion = new System.Version("19.1.0.25");
		this.mTargetEnvironment = OpenSpan.Design.TargetEnvironment.Driver;
	}
	
	// DesignForm-8D892C93CBDD3E5
	public SGEnrollment_Windows_Form1 Windows_Form1
	{
		get
		{
			return ((SGEnrollment_Windows_Form1)(this["Windows_Form1"]));
		}
	}
	
	// GlobalContainer-8D892BF0C8B166B
	public SGEnrollment__GC _GC
	{
		get
		{
			return ((SGEnrollment__GC)(this["_GC"]));
		}
	}
	
	// WebAdapter-8D892BE8D490EE1
	public SGEnrollment_Amisys Amisys
	{
		get
		{
			return ((SGEnrollment_Amisys)(this["Amisys"]));
		}
	}
	
	// Automator-8D8983C658F1AC2
	public SGEnrollment_SGE_P_BR0200_BrokerInfo SGE_P_BR0200_BrokerInfo
	{
		get
		{
			return ((SGEnrollment_SGE_P_BR0200_BrokerInfo)(this["SGE_P_BR0200_BrokerInfo"]));
		}
	}
	
	// Automator-8D8983C31C79D5E
	public SGEnrollment_SGE_P_IR0600_1095Info SGE_P_IR0600_1095Info
	{
		get
		{
			return ((SGEnrollment_SGE_P_IR0600_1095Info)(this["SGE_P_IR0600_1095Info"]));
		}
	}
	
	// Automator-8D8983B67060576
	public SGEnrollment_SGE_P_ME0210_GroupBilling SGE_P_ME0210_GroupBilling
	{
		get
		{
			return ((SGEnrollment_SGE_P_ME0210_GroupBilling)(this["SGE_P_ME0210_GroupBilling"]));
		}
	}
	
	// Automator-8D8983C4F601BE8
	public SGEnrollment_SGE_P_ME0300_DivisionSetup SGE_P_ME0300_DivisionSetup
	{
		get
		{
			return ((SGEnrollment_SGE_P_ME0300_DivisionSetup)(this["SGE_P_ME0300_DivisionSetup"]));
		}
	}
	
	// Automator-8D8983BB3BD6A25
	public SGEnrollment_SGE_P_RF0600_Address SGE_P_RF0600_Address
	{
		get
		{
			return ((SGEnrollment_SGE_P_RF0600_Address)(this["SGE_P_RF0600_Address"]));
		}
	}
	
	// Automator-8D8983BEFA70B17
	public SGEnrollment_SGE_P_RM0100_Remarks SGE_P_RM0100_Remarks
	{
		get
		{
			return ((SGEnrollment_SGE_P_RM0100_Remarks)(this["SGE_P_RM0100_Remarks"]));
		}
	}
	
	// Automator-8D89A57DE750AE9
	public SGEnrollment_SGE_A_BotActivity SGE_A_BotActivity
	{
		get
		{
			return ((SGEnrollment_SGE_A_BotActivity)(this["SGE_A_BotActivity"]));
		}
	}
	
	// Automator-8D8983B52328C2A
	public SGEnrollment_SGE_P_ME0200_NewGroupSetup SGE_P_ME0200_NewGroupSetup
	{
		get
		{
			return ((SGEnrollment_SGE_P_ME0200_NewGroupSetup)(this["SGE_P_ME0200_NewGroupSetup"]));
		}
	}
	
	// Automator-8D89A57F438BA8E
	public SGEnrollment_SGE_E_BotStart SGE_E_BotStart
	{
		get
		{
			return ((SGEnrollment_SGE_E_BotStart)(this["SGE_E_BotStart"]));
		}
	}
	
	// Automator-8D89A5D8D99372E
	public SGEnrollment_SGE_E_BotStop SGE_E_BotStop
	{
		get
		{
			return ((SGEnrollment_SGE_E_BotStop)(this["SGE_E_BotStop"]));
		}
	}
	
	// Automator-8D89A5C39600A9A
	public SGEnrollment_SGE_P_AllCloseWindows SGE_P_AllCloseWindows
	{
		get
		{
			return ((SGEnrollment_SGE_P_AllCloseWindows)(this["SGE_P_AllCloseWindows"]));
		}
	}
	
	// Automator-8D892C8CA280D5F
	public SGEnrollment_SGE_P_AppLogin SGE_P_AppLogin
	{
		get
		{
			return ((SGEnrollment_SGE_P_AppLogin)(this["SGE_P_AppLogin"]));
		}
	}
	
	// Automator-8D89A602629B57C
	public SGEnrollment_SGE_P_DivisionSetup_Loop SGE_P_DivisionSetup_Loop
	{
		get
		{
			return ((SGEnrollment_SGE_P_DivisionSetup_Loop)(this["SGE_P_DivisionSetup_Loop"]));
		}
	}
	
	// Automator-8D892C9B1528E40
	public SGEnrollment_SGE_Main SGE_Main
	{
		get
		{
			return ((SGEnrollment_SGE_Main)(this["SGE_Main"]));
		}
	}
}

}


using System;

namespace SGEnrollment
{
/// <summary>
/// OpenSpan design component.
/// </summary>
// Automator-8D8983BEFA70B17
[OpenSpan.Design.ComponentIdentityAttribute("Automator-8D8983BEFA70B17")]
[System.ComponentModel.ToolboxItemAttribute(false)]
public sealed class SGE_P_RM0100_Remarks : OpenSpan.Automation.Automator
{
	
	public SGE_P_RM0100_Remarks()
	{
		// 
		// Global Initialization
		// 
		OpenSpan.Diagnostics.Diagnostic.Initialize();
		// 
		// Setup field members
		// 
		// 
		// Initialize design component
		// 
		this.Initialize();
		// 
		// Set design component Id
		// 
		this.Id = new OpenSpan.Design.ComponentIdentity("Automator-8D8983BEFA70B17");
		// 
		// SGE_P_RM0100_Remarks
		// 
		ComponentInfo.CodeDomData = @"OSVSXawIAALVSy2rDMBD8FZGz8SONkxjsQJoSCDm0NCF3SR6DqLQyehz0920uLSYl5NCyl2V2Z2df7c6a0RIoHGiwm/acRrAL1xHd7HUEnUZO+TYGa3hQ9tu1bsaKTbv1HkbodIcxyXvHAAeS8Ldc46V1WomMXeD8F7Nb5OXVMraLOkSHjhCD4zpjb1FoJY9IZ/sB6sRqxWtZL6vmaYFy3dzt7ad+1eTVYwJD3YumQi/Leb+WNR4d/t+0TskHmL9aVfHbfV4ScaPkbejK2CuNKfIcle6nUDH9rU8=";
		this.IsStartStoppable = false;
		this.LogData = true;
		this.LogEvents = true;
		this.LogFile = "";
		this.LogToFile = false;
		this.Name = "SGE_P_RM0100_Remarks";
		this.SuppressErrors = false;
		// 
		// Initialize CodeDom data
		// 
		this.CodeDomData = "OSVSXnAAAALNxLC3Jz00syczPc8lPLs1NzSux4+VSULDxS8xNVQhLzClNtVVSUtCHCAZnVsEFTQ0MDHQU" +
			"QCRc2j8pKzW5pBjG9cnMy4ZznPNzQYbD+cGlSe5FiQUZEAEbfSzuAAA=";
		// 
		// Add components
		// 
		this.mComponents = new System.Collections.Generic.List<System.ComponentModel.IComponent>(20);
	}
	
	private System.Collections.Generic.List<System.ComponentModel.IComponent> mComponents;
	
	public System.Collections.Generic.List<System.ComponentModel.IComponent> Components
	{
		get
		{
			return this.mComponents;
		}
	}
	
	/// <summary>
	/// Start design component.
	/// </summary>
	public override void Start()
	{
		base.Start();
	}
	
	/// <summary>
	/// Stop design component.
	/// </summary>
	public override void Stop()
	{
		base.Stop();
	}
}

}


using System;

namespace SGEnrollment
{
/// <summary>
/// OpenSpan design component.
/// </summary>
// Automator-8D8983BB3BD6A25
[OpenSpan.Design.ComponentIdentityAttribute("Automator-8D8983BB3BD6A25")]
[System.ComponentModel.ToolboxItemAttribute(false)]
public sealed class SGE_P_RF0600_Address : OpenSpan.Automation.Automator
{
	
	public SGE_P_RF0600_Address()
	{
		// 
		// Global Initialization
		// 
		OpenSpan.Diagnostics.Diagnostic.Initialize();
		// 
		// Setup field members
		// 
		// 
		// Initialize design component
		// 
		this.Initialize();
		// 
		// Set design component Id
		// 
		this.Id = new OpenSpan.Design.ComponentIdentity("Automator-8D8983BB3BD6A25");
		// 
		// SGE_P_RF0600_Address
		// 
		ComponentInfo.CodeDomData = @"OSVSXawIAALVSy2rDMBD8FZGz8aONmxjsQJpSCD20JCF3SR6DqLQyehz092kObTEJIYeWvSyzOzv7ajfWjJZAYUuDXbWHNIIduY7oZu8jaD9yytcxWMODsj+udTNWrNq19zBCpxuMSd4OAxxIwl9yjZfWaSUydoTzX8xunpdny9gm6hAdOkIMjuuMfUShlXxDOthPUCcWC17L+qlqHucol83V3vbJB5j/qv49+W/9qsmr+wSGuhdNhV6WD/1S1rh3tX+mVVy7z0sibpS8DJ0Zr0pjijxHpfspVEx/6wQ=";
		this.IsStartStoppable = false;
		this.LogData = true;
		this.LogEvents = true;
		this.LogFile = "";
		this.LogToFile = false;
		this.Name = "SGE_P_RF0600_Address";
		this.SuppressErrors = false;
		// 
		// Initialize CodeDom data
		// 
		this.CodeDomData = "OSVSXnAAAALNxLC3Jz00syczPc8lPLs1NzSux4+VSULDxS8xNVQhLzClNtVVSUtCHCAZnVsEFTQ0MDHQU" +
			"QCRc2j8pKzW5pBjG9cnMy4ZznPNzQYbD+cGlSe5FiQUZEAEbfSzuAAA=";
		// 
		// Add components
		// 
		this.mComponents = new System.Collections.Generic.List<System.ComponentModel.IComponent>(20);
	}
	
	private System.Collections.Generic.List<System.ComponentModel.IComponent> mComponents;
	
	public System.Collections.Generic.List<System.ComponentModel.IComponent> Components
	{
		get
		{
			return this.mComponents;
		}
	}
	
	/// <summary>
	/// Start design component.
	/// </summary>
	public override void Start()
	{
		base.Start();
	}
	
	/// <summary>
	/// Stop design component.
	/// </summary>
	public override void Stop()
	{
		base.Stop();
	}
}

}


using System;

namespace SGEnrollment
{
/// <summary>
/// OpenSpan design component.
/// </summary>
// GlobalContainer-8D892BF0C8B166B
[OpenSpan.Design.ComponentIdentityAttribute("GlobalContainer-8D892BF0C8B166B")]
[System.ComponentModel.ToolboxItemAttribute(false)]
[OpenSpan.Runtime.RuntimeReferenceAttribute("System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c5619" +
	"34e089")]
public sealed class _GC : OpenSpan.Automation.GlobalContainer
{
	
	[OpenSpan.Design.ComponentIdentityAttribute("GlobalContainer-8D892BF0C8B166B\\RobotActivity-8D892BEED5379D8")]
	public OpenSpan.Interactions.Controls.RobotActivity ract_SGEnrollment;
	
	[OpenSpan.Design.ComponentIdentityAttribute("GlobalContainer-8D892BF0C8B166B\\DiagnosticsLog-8D892BEE14518FB")]
	public OpenSpan.Controls.DiagnosticsLog diagnosticsLog1;
	
	[OpenSpan.Design.ComponentIdentityAttribute("GlobalContainer-8D892BF0C8B166B\\MessageDialog-8D892BEE149DDA8")]
	public OpenSpan.Controls.MessageDialog messageDialog1;
	
	[OpenSpan.Design.ComponentIdentityAttribute("GlobalContainer-8D892BF0C8B166B\\MessageManifest-8D892BEFBB2FAED")]
	public OpenSpan.ApplicationFramework.MessageManifest.MessageManifest messageManifest;
	
	[OpenSpan.Design.ComponentIdentityAttribute("GlobalContainer-8D892BF0C8B166B\\StringUtils-8D892BEE14C402F")]
	public OpenSpan.Controls.StringUtils stringUtils;
	
	[OpenSpan.Design.ComponentIdentityAttribute("GlobalContainer-8D892BF0C8B166B\\Script-8D892C7EE36F0CD")]
	public OpenSpan.Script.Custom.Script script;
	
	[OpenSpan.Design.ComponentIdentityAttribute("GlobalContainer-8D892BF0C8B166B\\Pause-8D892C7F19BAF9F")]
	public OpenSpan.Controls.Pause pause;
	
	[OpenSpan.Design.ComponentIdentityAttribute("GlobalContainer-8D892BF0C8B166B\\AsoManager-8D892C7FC2BEFE0")]
	public OpenSpan.ApplicationFramework.AssistedSignOn.AsoManager asoManager;
	
	public _GC()
	{
		// 
		// Global Initialization
		// 
		OpenSpan.Diagnostics.Diagnostic.Initialize();
		// 
		// Setup field members
		// 
		System.ComponentModel.ComponentResourceManager _resources_ = new System.ComponentModel.ComponentResourceManager(typeof(_GC));
		this.ract_SGEnrollment = new OpenSpan.Interactions.Controls.RobotActivity();
		this.diagnosticsLog1 = new OpenSpan.Controls.DiagnosticsLog();
		this.messageDialog1 = new OpenSpan.Controls.MessageDialog();
		this.messageManifest = new OpenSpan.ApplicationFramework.MessageManifest.MessageManifest();
		this.stringUtils = new OpenSpan.Controls.StringUtils();
		this.script = new OpenSpan.Script.Custom.Script();
		this.pause = new OpenSpan.Controls.Pause();
		this.asoManager = new OpenSpan.ApplicationFramework.AssistedSignOn.AsoManager();
		// 
		// Initialize design component
		// 
		this.Initialize();
		// 
		// Set design component Id
		// 
		this.Id = new OpenSpan.Design.ComponentIdentity("GlobalContainer-8D892BF0C8B166B");
		// 
		// Set component Ids
		// 
		this.SetId(this.ract_SGEnrollment, new OpenSpan.Design.ComponentIdentity("RobotActivity-8D892BEED5379D8"));
		this.SetId(this.diagnosticsLog1, new OpenSpan.Design.ComponentIdentity("DiagnosticsLog-8D892BEE14518FB"));
		this.SetId(this.messageDialog1, new OpenSpan.Design.ComponentIdentity("MessageDialog-8D892BEE149DDA8"));
		this.SetId(this.messageManifest, new OpenSpan.Design.ComponentIdentity("MessageManifest-8D892BEFBB2FAED"));
		this.SetId(this.stringUtils, new OpenSpan.Design.ComponentIdentity("StringUtils-8D892BEE14C402F"));
		this.SetId(this.script, new OpenSpan.Design.ComponentIdentity("Script-8D892C7EE36F0CD"));
		this.SetId(this.pause, new OpenSpan.Design.ComponentIdentity("Pause-8D892C7F19BAF9F"));
		this.SetId(this.asoManager, new OpenSpan.Design.ComponentIdentity("AsoManager-8D892C7FC2BEFE0"));
		// 
		// _GC
		// 
		ComponentInfo.CodeDomData = _resources_.GetString("__GC_1_");
		this.IsStartStoppable = false;
		this.Name = "_GC";
		// 
		// ract_SGEnrollment
		// 
		this.ract_SGEnrollment.ActivityName = "None";
		this.ract_SGEnrollment.ClassName = null;
		// 
		// messageDialog1
		// 
		this.messageDialog1.Caption = "Information";
		this.messageDialog1.Message = null;
		// 
		// asoManager
		// 
		this.asoManager.Enable = false;
		this.asoManager.QueueActivity = false;
		// 
		// Add components
		// 
		this.mComponents = new System.Collections.Generic.List<System.ComponentModel.IComponent>(20);
		this.Components.Add(this.ract_SGEnrollment);
		this.Components.Add(this.diagnosticsLog1);
		this.Components.Add(this.messageDialog1);
		this.Components.Add(this.messageManifest);
		this.Components.Add(this.stringUtils);
		this.Components.Add(this.script);
		this.Components.Add(this.pause);
		this.Components.Add(this.asoManager);
	}
	
	private System.Collections.Generic.List<System.ComponentModel.IComponent> mComponents;
	
	public System.Collections.Generic.List<System.ComponentModel.IComponent> Components
	{
		get
		{
			return this.mComponents;
		}
	}
	
	/// <summary>
	/// Start design component.
	/// </summary>
	public override void Start()
	{
		base.Start();
	}
	
	/// <summary>
	/// Stop design component.
	/// </summary>
	public override void Stop()
	{
		base.Stop();
	}
	
	internal OpenSpan.Interactions.Controls.RobotActivity Create_ract_SGEnrollment(out System.Collections.Generic.ICollection<System.ComponentModel.IComponent> components)
	{
		components = new System.Collections.Generic.List<System.ComponentModel.IComponent>();
		OpenSpan.Interactions.Controls.RobotActivity ract_SGEnrollment = new OpenSpan.Interactions.Controls.RobotActivity();
		this.SetId(ract_SGEnrollment, new OpenSpan.Design.ComponentIdentity("RobotActivity-8D892BEED5379D8"));
		ract_SGEnrollment.ActivityName = "None";
		ract_SGEnrollment.ClassName = null;
		// 
		// Result
		// 
		return ract_SGEnrollment;
	}
	
	internal OpenSpan.Controls.DiagnosticsLog Create_diagnosticsLog1(out System.Collections.Generic.ICollection<System.ComponentModel.IComponent> components)
	{
		components = new System.Collections.Generic.List<System.ComponentModel.IComponent>();
		OpenSpan.Controls.DiagnosticsLog diagnosticsLog1 = new OpenSpan.Controls.DiagnosticsLog();
		this.SetId(diagnosticsLog1, new OpenSpan.Design.ComponentIdentity("DiagnosticsLog-8D892BEE14518FB"));
		// 
		// Result
		// 
		return diagnosticsLog1;
	}
	
	internal OpenSpan.Controls.MessageDialog Create_messageDialog1(out System.Collections.Generic.ICollection<System.ComponentModel.IComponent> components)
	{
		components = new System.Collections.Generic.List<System.ComponentModel.IComponent>();
		OpenSpan.Controls.MessageDialog messageDialog1 = new OpenSpan.Controls.MessageDialog();
		this.SetId(messageDialog1, new OpenSpan.Design.ComponentIdentity("MessageDialog-8D892BEE149DDA8"));
		messageDialog1.Caption = "Information";
		messageDialog1.Message = null;
		// 
		// Result
		// 
		return messageDialog1;
	}
	
	internal OpenSpan.ApplicationFramework.MessageManifest.MessageManifest Create_messageManifest(out System.Collections.Generic.ICollection<System.ComponentModel.IComponent> components)
	{
		components = new System.Collections.Generic.List<System.ComponentModel.IComponent>();
		OpenSpan.ApplicationFramework.MessageManifest.MessageManifest messageManifest = new OpenSpan.ApplicationFramework.MessageManifest.MessageManifest();
		this.SetId(messageManifest, new OpenSpan.Design.ComponentIdentity("MessageManifest-8D892BEFBB2FAED"));
		// 
		// Result
		// 
		return messageManifest;
	}
	
	internal OpenSpan.Controls.StringUtils Create_stringUtils(out System.Collections.Generic.ICollection<System.ComponentModel.IComponent> components)
	{
		components = new System.Collections.Generic.List<System.ComponentModel.IComponent>();
		OpenSpan.Controls.StringUtils stringUtils = new OpenSpan.Controls.StringUtils();
		this.SetId(stringUtils, new OpenSpan.Design.ComponentIdentity("StringUtils-8D892BEE14C402F"));
		// 
		// Result
		// 
		return stringUtils;
	}
	
	internal OpenSpan.Script.Custom.Script Create_script(out System.Collections.Generic.ICollection<System.ComponentModel.IComponent> components)
	{
		components = new System.Collections.Generic.List<System.ComponentModel.IComponent>();
		OpenSpan.Script.Custom.Script script = new OpenSpan.Script.Custom.Script();
		this.SetId(script, new OpenSpan.Design.ComponentIdentity("Script-8D892C7EE36F0CD"));
		// 
		// Result
		// 
		return script;
	}
	
	internal OpenSpan.Controls.Pause Create_pause(out System.Collections.Generic.ICollection<System.ComponentModel.IComponent> components)
	{
		components = new System.Collections.Generic.List<System.ComponentModel.IComponent>();
		OpenSpan.Controls.Pause pause = new OpenSpan.Controls.Pause();
		this.SetId(pause, new OpenSpan.Design.ComponentIdentity("Pause-8D892C7F19BAF9F"));
		// 
		// Result
		// 
		return pause;
	}
	
	internal OpenSpan.ApplicationFramework.AssistedSignOn.AsoManager Create_asoManager(out System.Collections.Generic.ICollection<System.ComponentModel.IComponent> components)
	{
		components = new System.Collections.Generic.List<System.ComponentModel.IComponent>();
		OpenSpan.ApplicationFramework.AssistedSignOn.AsoManager asoManager = new OpenSpan.ApplicationFramework.AssistedSignOn.AsoManager();
		this.SetId(asoManager, new OpenSpan.Design.ComponentIdentity("AsoManager-8D892C7FC2BEFE0"));
		asoManager.Enable = false;
		asoManager.QueueActivity = false;
		// 
		// Result
		// 
		return asoManager;
	}
}

}


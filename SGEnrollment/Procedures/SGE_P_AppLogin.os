<OpenSpanDesignDocument Version="19.1.0.25" Serializer="2.0" Culture="en-US">
  <ComponentInfo>
    <Type Value="OpenSpan.Automation.Automator" />
    <Assembly Value="OpenSpan.Automation" />
    <AssemblyReferences>
      <Assembly Value="mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <Assembly Value="System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <Assembly Value="OpenSpan, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Adapters, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.ApplicationFramework, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Automation, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Controls, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
    </AssemblyReferences>
    <DynamicAssemblyReferences />
    <FileReferences />
    <BuildReferences />
  </ComponentInfo>
  <Component Version="1.0">
    <OpenSpan.Automation.Automator Name="SGE_P_AppLogin" Id="Automator-8D892C8CA280D5F">
      <AutomationDocument>
        <Name Value="" />
        <Size Value="5005, 5000" />
        <Objects>
          <ConnectionBlock>
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\EntryPoint-8D892C8D526D9C4" />
            <Left Value="146" />
            <Top Value="84" />
            <PartID Value="1" />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CA8FFF81D9" />
            <PartID Value="3" />
            <Left Value="340" />
            <Top Value="360" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_Login" />
            <Fittings>
              <IsCreated Collapsed="False" ActualText="IsCreated" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAAE9A4A24" />
            <PartID Value="5" />
            <Left Value="700" />
            <Top Value="440" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="txtj_username" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAC1ED46D6" />
            <PartID Value="7" />
            <Left Value="900" />
            <Top Value="440" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="txtj_password" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="PerformClick" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableMethod-8D892CB1E8FA501" />
            <PartID Value="9" />
            <Left Value="1100" />
            <Top Value="440" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="btnSubmit" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock type="OpenSpan.Automation.Design.ConnectionBlocks.MultiExitPointBlock">
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ExitPoint-8D892CB2FAFED94" />
            <Left Value="1263" />
            <Top Value="102" />
            <PartID Value="11" />
            <Title Value="Exit" />
            <EventName Value="" />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableProperties-8D89A587BDF9F81" />
            <PartID Value="14" />
            <Left Value="340" />
            <Top Value="80" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="Amisys" />
            <Fittings>
              <IsRunning Collapsed="False" ActualText="IsRunning" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Start" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A587BE46382" />
            <PartID Value="15" />
            <Left Value="520" />
            <Top Value="160" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="Amisys" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\LabelHost-8D89A58FCC08F74" />
            <Left Value="153" />
            <Top Value="362" />
            <PartID Value="18" />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Sleep" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A595F80ED70" />
            <PartID Value="20" />
            <Left Value="680" />
            <Top Value="100" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pause" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="GoToLabel" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\JumpHost-8D89A596F045225" />
            <PartID Value="23" />
            <Left Value="811" />
            <Top Value="114" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="Jump To" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="GetCredentials" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A5A4DFDE67D" />
            <PartID Value="27" />
            <Left Value="500" />
            <Top Value="420" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="asoManager" />
            <Fittings>
              <found Collapsed="False" ActualText="found" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\LabelHost-8D89A6090F8D1CD" />
            <Left Value="182" />
            <Top Value="685" />
            <PartID Value="32" />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="WaitForCreate" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A4806B0A" />
            <PartID Value="33" />
            <Left Value="400" />
            <Top Value="680" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="frmMessage" />
            <Fittings>
              <Result Collapsed="False" ActualText="Result" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="PerformClick" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A9259FFE" />
            <PartID Value="34" />
            <Left Value="600" />
            <Top Value="680" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="btnOK" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="GoToLabel" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\JumpHost-8D89A60BA292EA2" />
            <PartID Value="37" />
            <Left Value="1280" />
            <Top Value="380" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="Jump To" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\LabelHost-8D89A60D427B1A1" />
            <Left Value="1083" />
            <Top Value="102" />
            <PartID Value="38" />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="GoToLabel" />
            <ConnectableUniqueId Value="Automator-8D892C8CA280D5F\JumpHost-8D89A60E8E3D462" />
            <PartID Value="40" />
            <Left Value="780" />
            <Top Value="720" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="Jump To" />
            <OverriddenIds />
          </ConnectionBlock>
        </Objects>
        <Links>
          <Link PartID="8" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="5" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAAE9A4A24" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAAE9A4A24" />
            <To PartID="7" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAC1ED46D6" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAC1ED46D6" />
            <LinkPoints>
              <Point value="835, 469" />
              <Point value="845, 469" />
              <Point value="870, 469" />
              <Point value="870, 469" />
              <Point value="895, 469" />
              <Point value="905, 469" />
            </LinkPoints>
          </Link>
          <Link PartID="10" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="7" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAC1ED46D6" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAC1ED46D6" />
            <To PartID="9" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D892CB1E8FA501" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D892CB1E8FA501" />
            <LinkPoints>
              <Point value="1033, 469" />
              <Point value="1043, 469" />
              <Point value="1069, 469" />
              <Point value="1069, 469" />
              <Point value="1095, 469" />
              <Point value="1105, 469" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="12" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="3" ParentMemberName="IsCreated" DecisionValue="True" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CA8FFF81D9" />
            <To PartID="37" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\JumpHost-8D89A60BA292EA2" MemberComponentId="Automator-8D892C8CA280D5F\JumpHost-8D89A60BA292EA2" />
            <LinkPoints>
              <Point value="449, 420" />
              <Point value="459, 420" />
              <Point value="460, 420" />
              <Point value="460, 397" />
              <Point value="1273, 397" />
              <Point value="1283, 397" />
            </LinkPoints>
          </DecisionEventLink>
          <DecisionEventLink PartID="16" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="14" ParentMemberName="IsRunning" DecisionValue="False" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D89A587BDF9F81" />
            <To PartID="15" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A587BE46382" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A587BE46382" />
            <LinkPoints>
              <Point value="449, 155" />
              <Point value="459, 155" />
              <Point value="460, 155" />
              <Point value="460, 189" />
              <Point value="515, 189" />
              <Point value="525, 189" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="17" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="1" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\EntryPoint-8D892C8D526D9C4" MemberComponentId="Automator-8D892C8CA280D5F\EntryPoint-8D892C8D526D9C4" />
            <To PartID="14" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D89A587BDF9F81" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableProperties-8D89A587BDF9F81" />
            <LinkPoints>
              <Point value="254, 100" />
              <Point value="264, 100" />
              <Point value="268, 100" />
              <Point value="268, 109" />
              <Point value="335, 109" />
              <Point value="345, 109" />
            </LinkPoints>
          </Link>
          <Link PartID="19" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="18" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\LabelHost-8D89A58FCC08F74" MemberComponentId="Automator-8D892C8CA280D5F\LabelHost-8D89A58FCC08F74" />
            <To PartID="3" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CA8FFF81D9" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CA8FFF81D9" />
            <LinkPoints>
              <Point value="244, 378" />
              <Point value="254, 378" />
              <Point value="254, 378" />
              <Point value="254, 389" />
              <Point value="335, 389" />
              <Point value="345, 389" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="21" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="14" ParentMemberName="IsRunning" DecisionValue="True" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D89A587BDF9F81" />
            <To PartID="20" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A595F80ED70" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A595F80ED70" />
            <LinkPoints>
              <Point value="449, 140" />
              <Point value="459, 140" />
              <Point value="460, 140" />
              <Point value="460, 129" />
              <Point value="675, 129" />
              <Point value="685, 129" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="22" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="15" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A587BE46382" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A587BE46382" />
            <To PartID="20" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A595F80ED70" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A595F80ED70" />
            <LinkPoints>
              <Point value="613, 189" />
              <Point value="623, 189" />
              <Point value="628, 189" />
              <Point value="628, 129" />
              <Point value="675, 129" />
              <Point value="685, 129" />
            </LinkPoints>
          </Link>
          <Link PartID="24" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="20" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A595F80ED70" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A595F80ED70" />
            <To PartID="23" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\JumpHost-8D89A596F045225" MemberComponentId="Automator-8D892C8CA280D5F\JumpHost-8D89A596F045225" />
            <LinkPoints>
              <Point value="766, 129" />
              <Point value="776, 129" />
              <Point value="776, 131" />
              <Point value="776, 131" />
              <Point value="804, 131" />
              <Point value="814, 131" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="28" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="27" ParentMemberName="found" DecisionValue="True" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A5A4DFDE67D" />
            <To PartID="5" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAAE9A4A24" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAAE9A4A24" />
            <LinkPoints>
              <Point value="637, 549" />
              <Point value="647, 549" />
              <Point value="644, 549" />
              <Point value="644, 549" />
              <Point value="652, 549" />
              <Point value="652, 469" />
              <Point value="695, 469" />
              <Point value="705, 469" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="29" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="27" PortName="userName" PortType="Property" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A5A4DFDE67D" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A5A4DFDE67D" />
            <To PartID="5" PortName="Text" PortType="Property" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAAE9A4A24" MemberComponentId="WebAdapter-8D892BE8D490EE1\TextBox-8D892C6800BE3A7" />
            <LinkPoints>
              <Point value="637, 500" />
              <Point value="647, 500" />
              <Point value="652, 500" />
              <Point value="652, 486" />
              <Point value="695, 486" />
              <Point value="705, 486" />
            </LinkPoints>
          </Link>
          <Link PartID="30" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="27" PortName="password" PortType="Property" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A5A4DFDE67D" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A5A4DFDE67D" />
            <To PartID="7" PortName="Text" PortType="Property" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CAC1ED46D6" MemberComponentId="WebAdapter-8D892BE8D490EE1\TextBox-8D892C685F02CC2" />
            <LinkPoints>
              <Point value="637, 517" />
              <Point value="647, 517" />
              <Point value="652, 517" />
              <Point value="652, 517" />
              <Point value="852, 517" />
              <Point value="852, 486" />
              <Point value="895, 486" />
              <Point value="905, 486" />
            </LinkPoints>
          </Link>
          <Link PartID="35" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="32" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\LabelHost-8D89A6090F8D1CD" MemberComponentId="Automator-8D892C8CA280D5F\LabelHost-8D89A6090F8D1CD" />
            <To PartID="33" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A4806B0A" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A4806B0A" />
            <LinkPoints>
              <Point value="278, 701" />
              <Point value="288, 701" />
              <Point value="341, 701" />
              <Point value="341, 709" />
              <Point value="395, 709" />
              <Point value="405, 709" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="36" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="33" ParentMemberName="Result" DecisionValue="True" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A4806B0A" />
            <To PartID="34" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A9259FFE" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A9259FFE" />
            <LinkPoints>
              <Point value="535, 740" />
              <Point value="545, 740" />
              <Point value="548, 740" />
              <Point value="548, 709" />
              <Point value="595, 709" />
              <Point value="605, 709" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="39" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="38" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\LabelHost-8D89A60D427B1A1" MemberComponentId="Automator-8D892C8CA280D5F\LabelHost-8D89A60D427B1A1" />
            <To PartID="11" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ExitPoint-8D892CB2FAFED94" MemberComponentId="Automator-8D892C8CA280D5F\ExitPoint-8D892CB2FAFED94" />
            <LinkPoints>
              <Point value="1161, 120" />
              <Point value="1171, 120" />
              <Point value="1213, 120" />
              <Point value="1213, 120" />
              <Point value="1255, 120" />
              <Point value="1265, 120" />
            </LinkPoints>
          </Link>
          <Link PartID="41" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="34" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A9259FFE" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A9259FFE" />
            <To PartID="40" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\JumpHost-8D89A60E8E3D462" MemberComponentId="Automator-8D892C8CA280D5F\JumpHost-8D89A60E8E3D462" />
            <LinkPoints>
              <Point value="724, 709" />
              <Point value="734, 709" />
              <Point value="732, 709" />
              <Point value="732, 709" />
              <Point value="740, 709" />
              <Point value="740, 737" />
              <Point value="773, 737" />
              <Point value="783, 737" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="42" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="33" ParentMemberName="Result" DecisionValue="False" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A60A4806B0A" />
            <To PartID="40" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\JumpHost-8D89A60E8E3D462" MemberComponentId="Automator-8D892C8CA280D5F\JumpHost-8D89A60E8E3D462" />
            <LinkPoints>
              <Point value="535, 755" />
              <Point value="545, 755" />
              <Point value="548, 755" />
              <Point value="548, 737" />
              <Point value="773, 737" />
              <Point value="783, 737" />
            </LinkPoints>
          </DecisionEventLink>
          <DecisionEventLink PartID="43" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="3" ParentMemberName="IsCreated" DecisionValue="False" ConnectableId="Automator-8D892C8CA280D5F\ConnectableProperties-8D892CA8FFF81D9" />
            <To PartID="27" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A5A4DFDE67D" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D89A5A4DFDE67D" />
            <LinkPoints>
              <Point value="449, 435" />
              <Point value="459, 435" />
              <Point value="460, 435" />
              <Point value="460, 449" />
              <Point value="495, 449" />
              <Point value="505, 449" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="44" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="9" PortName="Complete" PortType="Event" ConnectableId="Automator-8D892C8CA280D5F\ConnectableMethod-8D892CB1E8FA501" MemberComponentId="Automator-8D892C8CA280D5F\ConnectableMethod-8D892CB1E8FA501" />
            <To PartID="37" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D892C8CA280D5F\JumpHost-8D89A60BA292EA2" MemberComponentId="Automator-8D892C8CA280D5F\JumpHost-8D89A60BA292EA2" />
            <LinkPoints>
              <Point value="1224, 469" />
              <Point value="1234, 469" />
              <Point value="1236, 469" />
              <Point value="1236, 397" />
              <Point value="1273, 397" />
              <Point value="1283, 397" />
            </LinkPoints>
          </Link>
        </Links>
        <Comments />
        <SubGraphs />
      </AutomationDocument>
      <DocumentPosition Value="Binary">
        <Binary>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5Qb2ludEYCAAAAAXgBeQAACwsCAAAAAAAAAAAAUEML</Binary>
      </DocumentPosition>
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicMethodInfo dynamicType="Method" name="_EntryPointExecute" aliasName="Execute" visibility="DefaultOn" source="" blockTypeName="OpenSpan.Automation.Design.ConnectionBlocks.EntryPointExecuteBlock" returnType="System.Void" />
        </Items>
      </Content>
    </OpenSpan.Automation.Automator>
    <OpenSpan.Automation.EntryPoint Name="entryPoint1" Id="EntryPoint-8D892C8D526D9C4">
      <AliasName Value="Execute" />
      <ComponentName Value="Execute" />
      <DisplayName Value="Execute" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.EntryPoint" />
      <InstanceUniqueId Value="Automator-8D892C8CA280D5F\EntryPoint-8D892C8D526D9C4" />
      <MemberDetails Value="" />
      <MethodName Value="_EntryPointExecute" />
      <Removing Value="False" />
      <UseAlias Value="True" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="Result" canRead="True" canWrite="True" type="System.Void" aliasName="Result" shouldSerialize="False" visibility="AlwaysHidden" source="" blockTypeName="" />
        </Items>
      </Content>
    </OpenSpan.Automation.EntryPoint>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties1" Id="ConnectableProperties-8D892CA8FFF81D9">
      <ComponentName Value="pg_Login" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D892C6801A3203" />
      <MemberDetails Value=".IsCreated Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="IsCreated" />
            <MemberType Value="Property" />
            <TypeName Value="System.Boolean" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties2" Id="ConnectableProperties-8D892CAAE9A4A24">
      <ComponentName Value="txtj_username" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.TextBox" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\TextBox-8D892C6800BE3A7" />
      <MemberDetails Value=".Text Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="Text" />
            <MemberType Value="Property" />
            <TypeName Value="System.String" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties4" Id="ConnectableProperties-8D892CAC1ED46D6">
      <ComponentName Value="txtj_password" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.TextBox" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\TextBox-8D892C685F02CC2" />
      <MemberDetails Value=".Text Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="Text" />
            <MemberType Value="Property" />
            <TypeName Value="System.String" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod1" Id="ConnectableMethod-8D892CB1E8FA501">
      <ComponentName Value="btnSubmit" />
      <DisplayName Value="PerformClick" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.Button" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\Button-8D892C68B170C58" />
      <MemberDetails Value=".PerformClick() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="PerformClick" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ExitPoint Name="exitPoint1" Id="ExitPoint-8D892CB2FAFED94">
      <ComponentName Value="Execute" />
      <DisplayName Value="Exit" />
      <EntryPoint Value="ComponentReference" Name="entryPoint1" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.EntryPoint" />
      <InstanceUniqueId Value="Automator-8D892C8CA280D5F\EntryPoint-8D892C8D526D9C4" />
      <MemberDetails Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="Result" canRead="True" canWrite="True" type="System.Void" aliasName="Result" shouldSerialize="False" visibility="DefaultOn" source="" blockTypeName="" />
        </Items>
      </Content>
    </OpenSpan.Automation.ExitPoint>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties3" Id="ConnectableProperties-8D89A587BDF9F81">
      <ComponentName Value="Amisys" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.WebAdapter" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1" />
      <MemberDetails Value=".IsRunning Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="IsRunning" />
            <MemberType Value="Property" />
            <TypeName Value="System.Boolean" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod2" Id="ConnectableMethod-8D89A587BE46382">
      <ComponentName Value="Amisys" />
      <DisplayName Value="Start" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.WebAdapter" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1" />
      <MemberDetails Value=".Start() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="Start" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.LabelHost Name="labelHost1" Id="LabelHost-8D89A58FCC08F74">
      <ComponentName Value="OpenSpan.Automation.EntryPoint" />
      <DisplayName Value="Login" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.EntryPoint" />
      <InstanceUniqueId Value="EMPTY" />
      <LabelName Value="Login" />
      <MemberDetails Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicMethodInfo dynamicType="Method" name="GoToLabel" aliasName="GoToLabel" visibility="AlwaysHidden" source="" blockTypeName="" returnType="System.Void" />
        </Items>
      </Content>
    </OpenSpan.Automation.LabelHost>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod3" Id="ConnectableMethod-8D89A595F80ED70">
      <ComponentName Value="pause" />
      <DisplayName Value="Sleep" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Controls.Pause" />
      <InstanceUniqueId Value="GlobalContainer-8D892BF0C8B166B\Pause-8D892C7F19BAF9F" />
      <MemberDetails Value=".Sleep() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="Sleep" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
                <Content Name="ParameterPrototype">
                  <Items>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="False" />
                      <CanWrite Value="True" />
                      <DefaultSet Value="True" />
                      <DefaultValue Value="3000" />
                      <ParamName Value="milliseconds" />
                      <Position Value="0" />
                      <TypeName Value="System.Int32" />
                    </OpenSpan.Automation.ParameterPrototype>
                  </Items>
                </Content>
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.JumpHost Name="jumpHost1" Id="JumpHost-8D89A596F045225">
      <ComponentName Value="labelHost1" />
      <DisplayName Value="GoToLabel" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.LabelHost" />
      <InstanceUniqueId Value="Automator-8D892C8CA280D5F\LabelHost-8D89A58FCC08F74" />
      <MemberDetails Value=" - Login" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="GoToLabel" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.JumpHost>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod4" Id="ConnectableMethod-8D89A5A4DFDE67D">
      <ComponentName Value="asoManager" />
      <DisplayName Value="GetCredentials" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.ApplicationFramework.AssistedSignOn.AsoManager" />
      <InstanceUniqueId Value="GlobalContainer-8D892BF0C8B166B\AsoManager-8D892C7FC2BEFE0" />
      <MemberDetails Value=".GetCredentials() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="GetCredentials" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
                <Content Name="ParameterPrototype">
                  <Items>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="False" />
                      <CanWrite Value="True" />
                      <DefaultSet Value="True" />
                      <DefaultValue Value="Amisys" />
                      <ParamName Value="applicationKey" />
                      <Position Value="0" />
                      <TypeName Value="System.String" />
                    </OpenSpan.Automation.ParameterPrototype>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="True" />
                      <CanWrite Value="False" />
                      <DefaultSet Value="False" />
                      <ParamName Value="domain" />
                      <Position Value="1" />
                      <TypeName Value="System.String" />
                    </OpenSpan.Automation.ParameterPrototype>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="True" />
                      <CanWrite Value="False" />
                      <DefaultSet Value="False" />
                      <ParamName Value="userName" />
                      <Position Value="2" />
                      <TypeName Value="System.String" />
                    </OpenSpan.Automation.ParameterPrototype>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="True" />
                      <CanWrite Value="False" />
                      <DefaultSet Value="False" />
                      <ParamName Value="password" />
                      <Position Value="3" />
                      <TypeName Value="System.String" />
                    </OpenSpan.Automation.ParameterPrototype>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="True" />
                      <CanWrite Value="False" />
                      <DefaultSet Value="False" />
                      <ParamName Value="found" />
                      <Position Value="4" />
                      <TypeName Value="System.Boolean" />
                    </OpenSpan.Automation.ParameterPrototype>
                  </Items>
                </Content>
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.LabelHost Name="labelHost2" Id="LabelHost-8D89A6090F8D1CD">
      <ComponentName Value="OpenSpan.Automation.EntryPoint" />
      <DisplayName Value="popup" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.EntryPoint" />
      <InstanceUniqueId Value="EMPTY" />
      <LabelName Value="popup" />
      <MemberDetails Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicMethodInfo dynamicType="Method" name="GoToLabel" aliasName="GoToLabel" visibility="AlwaysHidden" source="" blockTypeName="" returnType="System.Void" />
        </Items>
      </Content>
    </OpenSpan.Automation.LabelHost>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod5" Id="ConnectableMethod-8D89A60A4806B0A">
      <ComponentName Value="frmMessage" />
      <DisplayName Value="WaitForCreate" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Controls.Form" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\Form-8D894CB04175166" />
      <MemberDetails Value=".WaitForCreate() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="Result" canRead="True" canWrite="False" type="System.Boolean" aliasName="Result" shouldSerialize="False" visibility="DefaultOn" source="" blockTypeName="" />
        </Items>
      </Content>
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="WaitForCreate" />
            <MemberType Value="Method" />
            <TypeName Value="System.Boolean" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Boolean" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod6" Id="ConnectableMethod-8D89A60A9259FFE">
      <ComponentName Value="btnOK" />
      <DisplayName Value="PerformClick" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Controls.Button" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\Button-8D894CB04102A21" />
      <MemberDetails Value=".PerformClick() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="PerformClick" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.JumpHost Name="jumpHost2" Id="JumpHost-8D89A60BA292EA2">
      <ComponentName Value="labelHost2" />
      <DisplayName Value="GoToLabel" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.LabelHost" />
      <InstanceUniqueId Value="Automator-8D892C8CA280D5F\LabelHost-8D89A6090F8D1CD" />
      <MemberDetails Value=" - popup" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="GoToLabel" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.JumpHost>
    <OpenSpan.Automation.LabelHost Name="labelHost3" Id="LabelHost-8D89A60D427B1A1">
      <ComponentName Value="OpenSpan.Automation.EntryPoint" />
      <DisplayName Value="exit" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.EntryPoint" />
      <InstanceUniqueId Value="EMPTY" />
      <LabelName Value="exit" />
      <MemberDetails Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicMethodInfo dynamicType="Method" name="GoToLabel" aliasName="GoToLabel" visibility="AlwaysHidden" source="" blockTypeName="" returnType="System.Void" />
        </Items>
      </Content>
    </OpenSpan.Automation.LabelHost>
    <OpenSpan.Automation.JumpHost Name="jumpHost3" Id="JumpHost-8D89A60E8E3D462">
      <ComponentName Value="labelHost3" />
      <DisplayName Value="GoToLabel" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.LabelHost" />
      <InstanceUniqueId Value="Automator-8D892C8CA280D5F\LabelHost-8D89A60D427B1A1" />
      <MemberDetails Value=" - exit" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="GoToLabel" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.JumpHost>
  </Component>
</OpenSpanDesignDocument>
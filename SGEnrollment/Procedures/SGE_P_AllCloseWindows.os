<OpenSpanDesignDocument Version="19.1.0.25" Serializer="2.0" Culture="en-US">
  <ComponentInfo>
    <Type Value="OpenSpan.Automation.Automator" />
    <Assembly Value="OpenSpan.Automation" />
    <AssemblyReferences>
      <Assembly Value="mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <Assembly Value="OpenSpan, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Adapters.Web, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Automation, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    </AssemblyReferences>
    <DynamicAssemblyReferences />
    <FileReferences />
    <BuildReferences />
  </ComponentInfo>
  <Component Version="1.0">
    <OpenSpan.Automation.Automator Name="SGE_P_AllCloseWindows" Id="Automator-8D89A5C39600A9A">
      <AutomationDocument>
        <Name Value="" />
        <Size Value="5005, 5000" />
        <Objects>
          <ConnectionBlock>
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\EntryPoint-8D89A5C42440183" />
            <Left Value="140" />
            <Top Value="260" />
            <PartID Value="1" />
          </ConnectionBlock>
          <ConnectionBlock>
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\LabelHost-8D89A5C4F753C33" />
            <Left Value="160" />
            <Top Value="60" />
            <PartID Value="2" />
          </ConnectionBlock>
          <ConnectionBlock type="OpenSpan.Automation.Design.ConnectionBlocks.MultiExitPointBlock">
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ExitPoint-8D89A5C55B3554A" />
            <Left Value="340" />
            <Top Value="60" />
            <PartID Value="3" />
            <Title Value="Exit" />
            <EventName Value="" />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D17960E12" />
            <PartID Value="7" />
            <Left Value="340" />
            <Top Value="260" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_ME0200" />
            <Fittings>
              <IsCreated Collapsed="False" ActualText="IsCreated" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="CloseWindow" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D30BDE399" />
            <PartID Value="8" />
            <Left Value="520" />
            <Top Value="260" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_ME0200" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="CloseWindow" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D367D8487" />
            <PartID Value="9" />
            <Left Value="860" />
            <Top Value="340" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_ME0210" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D39D78F58" />
            <PartID Value="10" />
            <Left Value="680" />
            <Top Value="340" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_ME0210" />
            <Fittings>
              <IsCreated Collapsed="False" ActualText="IsCreated" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D3E873511" />
            <PartID Value="11" />
            <Left Value="1020" />
            <Top Value="420" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_ME0300" />
            <Fittings>
              <IsCreated Collapsed="False" ActualText="IsCreated" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="CloseWindow" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D42904172" />
            <PartID Value="12" />
            <Left Value="1200" />
            <Top Value="420" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_ME0300" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="CloseWindow" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D4A1581C9" />
            <PartID Value="13" />
            <Left Value="1880" />
            <Top Value="560" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_RF0600" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D4E628963" />
            <PartID Value="14" />
            <Left Value="1700" />
            <Top Value="560" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_RF0600" />
            <Fittings>
              <IsCreated Collapsed="False" ActualText="IsCreated" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D54BA21B4" />
            <PartID Value="15" />
            <Left Value="1360" />
            <Top Value="500" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_RM0100" />
            <Fittings>
              <IsCreated Collapsed="False" ActualText="IsCreated" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="CloseWindow" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D59276C4B" />
            <PartID Value="16" />
            <Left Value="1520" />
            <Top Value="500" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_RM0100" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="CloseWindow" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D63D428E0" />
            <PartID Value="17" />
            <Left Value="2220" />
            <Top Value="640" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_BR0200" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D67BA91DC" />
            <PartID Value="18" />
            <Left Value="2040" />
            <Top Value="640" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_BR0200" />
            <Fittings>
              <IsCreated Collapsed="False" ActualText="IsCreated" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="Properties" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D6B891DA1" />
            <PartID Value="19" />
            <Left Value="2400" />
            <Top Value="720" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_1095Info" />
            <Fittings>
              <IsCreated Collapsed="False" ActualText="IsCreated" />
            </Fittings>
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="CloseWindow" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D7071FF30" />
            <PartID Value="20" />
            <Left Value="2600" />
            <Top Value="720" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="pg_1095Info" />
            <OverriddenIds />
          </ConnectionBlock>
          <ConnectionBlock>
            <DisplayName Value="GoToLabel" />
            <ConnectableUniqueId Value="Automator-8D89A5C39600A9A\JumpHost-8D89A5F58EE740D" />
            <PartID Value="36" />
            <Left Value="2800" />
            <Top Value="720" />
            <Collapsed Value="False" />
            <WillExecute Value="True" />
            <InstanceName Value="Jump To" />
            <OverriddenIds />
          </ConnectionBlock>
        </Objects>
        <Links>
          <Link PartID="4" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="2" PortName="Complete" PortType="Event" ConnectableId="Automator-8D89A5C39600A9A\LabelHost-8D89A5C4F753C33" MemberComponentId="Automator-8D89A5C39600A9A\LabelHost-8D89A5C4F753C33" />
            <To PartID="3" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ExitPoint-8D89A5C55B3554A" MemberComponentId="Automator-8D89A5C39600A9A\ExitPoint-8D89A5C55B3554A" />
            <LinkPoints>
              <Point value="256, 78" />
              <Point value="266, 78" />
              <Point value="266, 78" />
              <Point value="266, 78" />
              <Point value="332, 78" />
              <Point value="342, 78" />
            </LinkPoints>
          </Link>
          <Link PartID="5" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="2" PortName="_param1" PortType="Property" ConnectableId="Automator-8D89A5C39600A9A\LabelHost-8D89A5C4F753C33" MemberComponentId="EMPTY" />
            <To PartID="3" PortName="param1" PortType="Property" ConnectableId="Automator-8D89A5C39600A9A\ExitPoint-8D89A5C55B3554A" MemberComponentId="EMPTY" />
            <LinkPoints>
              <Point value="256, 105" />
              <Point value="266, 105" />
              <Point value="266, 105" />
              <Point value="266, 105" />
              <Point value="332, 105" />
              <Point value="342, 105" />
            </LinkPoints>
          </Link>
          <Link PartID="6" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="2" PortName="_param2" PortType="Property" ConnectableId="Automator-8D89A5C39600A9A\LabelHost-8D89A5C4F753C33" MemberComponentId="EMPTY" />
            <To PartID="3" PortName="_param1" PortType="Property" ConnectableId="Automator-8D89A5C39600A9A\ExitPoint-8D89A5C55B3554A" MemberComponentId="EMPTY" />
            <LinkPoints>
              <Point value="256, 121" />
              <Point value="266, 121" />
              <Point value="266, 121" />
              <Point value="266, 121" />
              <Point value="332, 121" />
              <Point value="342, 121" />
            </LinkPoints>
          </Link>
          <Link PartID="21" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="1" PortName="Complete" PortType="Event" ConnectableId="Automator-8D89A5C39600A9A\EntryPoint-8D89A5C42440183" MemberComponentId="Automator-8D89A5C39600A9A\EntryPoint-8D89A5C42440183" />
            <To PartID="7" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D17960E12" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D17960E12" />
            <LinkPoints>
              <Point value="251, 278" />
              <Point value="261, 278" />
              <Point value="298, 278" />
              <Point value="298, 289" />
              <Point value="335, 289" />
              <Point value="345, 289" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="22" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="7" ParentMemberName="IsCreated" DecisionValue="True" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D17960E12" />
            <To PartID="8" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D30BDE399" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D30BDE399" />
            <LinkPoints>
              <Point value="462, 320" />
              <Point value="472, 320" />
              <Point value="476, 320" />
              <Point value="476, 289" />
              <Point value="515, 289" />
              <Point value="525, 289" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="23" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="8" PortName="Complete" PortType="Event" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D30BDE399" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D30BDE399" />
            <To PartID="10" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D39D78F58" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D39D78F58" />
            <LinkPoints>
              <Point value="650, 289" />
              <Point value="660, 289" />
              <Point value="660, 289" />
              <Point value="660, 369" />
              <Point value="675, 369" />
              <Point value="685, 369" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="25" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="10" ParentMemberName="IsCreated" DecisionValue="True" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D39D78F58" />
            <To PartID="9" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D367D8487" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D367D8487" />
            <LinkPoints>
              <Point value="802, 400" />
              <Point value="812, 400" />
              <Point value="812, 400" />
              <Point value="812, 369" />
              <Point value="855, 369" />
              <Point value="865, 369" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="26" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="9" PortName="Complete" PortType="Event" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D367D8487" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D367D8487" />
            <To PartID="11" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D3E873511" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D3E873511" />
            <LinkPoints>
              <Point value="990, 369" />
              <Point value="1000, 369" />
              <Point value="1004, 369" />
              <Point value="1004, 449" />
              <Point value="1015, 449" />
              <Point value="1025, 449" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="28" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="11" ParentMemberName="IsCreated" DecisionValue="True" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D3E873511" />
            <To PartID="12" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D42904172" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D42904172" />
            <LinkPoints>
              <Point value="1142, 480" />
              <Point value="1152, 480" />
              <Point value="1156, 480" />
              <Point value="1156, 449" />
              <Point value="1195, 449" />
              <Point value="1205, 449" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="29" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="12" PortName="Complete" PortType="Event" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D42904172" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D42904172" />
            <To PartID="15" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D54BA21B4" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D54BA21B4" />
            <LinkPoints>
              <Point value="1330, 449" />
              <Point value="1340, 449" />
              <Point value="1340, 449" />
              <Point value="1340, 529" />
              <Point value="1355, 529" />
              <Point value="1365, 529" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="31" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="15" ParentMemberName="IsCreated" DecisionValue="True" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D54BA21B4" />
            <To PartID="16" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D59276C4B" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D59276C4B" />
            <LinkPoints>
              <Point value="1483, 560" />
              <Point value="1493, 560" />
              <Point value="1493, 560" />
              <Point value="1493, 529" />
              <Point value="1515, 529" />
              <Point value="1525, 529" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="32" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="16" PortName="Complete" PortType="Event" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D59276C4B" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D59276C4B" />
            <To PartID="14" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D4E628963" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D4E628963" />
            <LinkPoints>
              <Point value="1650, 529" />
              <Point value="1660, 529" />
              <Point value="1660, 529" />
              <Point value="1660, 589" />
              <Point value="1695, 589" />
              <Point value="1705, 589" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="34" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="14" ParentMemberName="IsCreated" DecisionValue="True" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D4E628963" />
            <To PartID="13" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D4A1581C9" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D4A1581C9" />
            <LinkPoints>
              <Point value="1820, 620" />
              <Point value="1830, 620" />
              <Point value="1828, 620" />
              <Point value="1828, 620" />
              <Point value="1836, 620" />
              <Point value="1836, 589" />
              <Point value="1875, 589" />
              <Point value="1885, 589" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="38" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="13" PortName="Complete" PortType="Event" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D4A1581C9" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D4A1581C9" />
            <To PartID="18" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D67BA91DC" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D67BA91DC" />
            <LinkPoints>
              <Point value="2010, 589" />
              <Point value="2020, 589" />
              <Point value="2020, 589" />
              <Point value="2020, 669" />
              <Point value="2035, 669" />
              <Point value="2045, 669" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="39" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="14" ParentMemberName="IsCreated" DecisionValue="False" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D4E628963" />
            <To PartID="18" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D67BA91DC" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D67BA91DC" />
            <LinkPoints>
              <Point value="1820, 635" />
              <Point value="1830, 635" />
              <Point value="1836, 635" />
              <Point value="1836, 669" />
              <Point value="2035, 669" />
              <Point value="2045, 669" />
            </LinkPoints>
          </DecisionEventLink>
          <DecisionEventLink PartID="40" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="18" ParentMemberName="IsCreated" DecisionValue="True" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D67BA91DC" />
            <To PartID="17" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D63D428E0" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D63D428E0" />
            <LinkPoints>
              <Point value="2161, 700" />
              <Point value="2171, 700" />
              <Point value="2172, 700" />
              <Point value="2172, 669" />
              <Point value="2215, 669" />
              <Point value="2225, 669" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="41" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="17" PortName="Complete" PortType="Event" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D63D428E0" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D63D428E0" />
            <To PartID="19" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D6B891DA1" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D6B891DA1" />
            <LinkPoints>
              <Point value="2350, 669" />
              <Point value="2360, 669" />
              <Point value="2364, 669" />
              <Point value="2364, 749" />
              <Point value="2395, 749" />
              <Point value="2405, 749" />
            </LinkPoints>
          </Link>
          <DecisionEventLink PartID="42" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="18" ParentMemberName="IsCreated" DecisionValue="False" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D67BA91DC" />
            <To PartID="19" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D6B891DA1" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D6B891DA1" />
            <LinkPoints>
              <Point value="2161, 715" />
              <Point value="2171, 715" />
              <Point value="2172, 715" />
              <Point value="2172, 749" />
              <Point value="2395, 749" />
              <Point value="2405, 749" />
            </LinkPoints>
          </DecisionEventLink>
          <DecisionEventLink PartID="43" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="19" ParentMemberName="IsCreated" DecisionValue="True" ConnectableId="Automator-8D89A5C39600A9A\ConnectableProperties-8D89A5D6B891DA1" />
            <To PartID="20" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D7071FF30" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D7071FF30" />
            <LinkPoints>
              <Point value="2525, 780" />
              <Point value="2535, 780" />
              <Point value="2540, 780" />
              <Point value="2540, 749" />
              <Point value="2595, 749" />
              <Point value="2605, 749" />
            </LinkPoints>
          </DecisionEventLink>
          <Link PartID="44" Sensitive="False" Asynchronous="False" LogBeforeExecution="" LogAfterExecution="">
            <From PartID="20" PortName="Complete" PortType="Event" ConnectableId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D7071FF30" MemberComponentId="Automator-8D89A5C39600A9A\ConnectableMethod-8D89A5D7071FF30" />
            <To PartID="36" PortName="DoWork" PortType="Method" ConnectableId="Automator-8D89A5C39600A9A\JumpHost-8D89A5F58EE740D" MemberComponentId="Automator-8D89A5C39600A9A\JumpHost-8D89A5F58EE740D" />
            <LinkPoints>
              <Point value="2730, 749" />
              <Point value="2740, 749" />
              <Point value="2740, 749" />
              <Point value="2740, 737" />
              <Point value="2793, 737" />
              <Point value="2803, 737" />
            </LinkPoints>
          </Link>
        </Links>
        <Comments />
        <SubGraphs />
      </AutomationDocument>
      <DocumentPosition Value="Binary">
        <Binary>AAEAAAD/////AQAAAAAAAAAMAgAAAFFTeXN0ZW0uRHJhd2luZywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EFAQAAABVTeXN0ZW0uRHJhd2luZy5Qb2ludEYCAAAAAXgBeQAACwsCAAAAANANRQAA+EML</Binary>
      </DocumentPosition>
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicMethodInfo dynamicType="Method" name="_EntryPointExecute" aliasName="Execute" visibility="DefaultOn" source="" blockTypeName="OpenSpan.Automation.Design.ConnectionBlocks.EntryPointExecuteBlock" returnType="System.Void">
            <param name="param1" aliasName="Result" paramType="System.Boolean" isIn="False" isOut="True" position="0" />
            <param name="_param1" aliasName="Msg" paramType="System.String" isIn="False" isOut="True" position="1" />
          </OpenSpan.DynamicMembers.DynamicMethodInfo>
        </Items>
      </Content>
    </OpenSpan.Automation.Automator>
    <OpenSpan.Automation.EntryPoint Name="entryPoint1" Id="EntryPoint-8D89A5C42440183">
      <AliasName Value="Execute" />
      <ComponentName Value="&lt;No Instance&gt;" />
      <DisplayName Value="" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.EntryPoint" />
      <InstanceUniqueId Value="Automator-8D89A5C39600A9A\EntryPoint-8D89A5C42440183" />
      <MemberDetails Value="" />
      <MethodName Value="_EntryPointExecute" />
      <Removing Value="False" />
      <UseAlias Value="True" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="Result" canRead="True" canWrite="True" type="System.Void" aliasName="Result" shouldSerialize="False" visibility="AlwaysHidden" source="" blockTypeName="" />
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="param1" canRead="True" canWrite="False" type="System.Boolean" aliasName="Result" shouldSerialize="False" visibility="AlwaysHidden" source="" blockTypeName="" />
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="_param1" canRead="True" canWrite="False" type="System.String" aliasName="Msg" shouldSerialize="False" visibility="AlwaysHidden" source="" blockTypeName="" />
        </Items>
      </Content>
    </OpenSpan.Automation.EntryPoint>
    <OpenSpan.Automation.LabelHost Name="labelHost1" Id="LabelHost-8D89A5C4F753C33">
      <ComponentName Value="OpenSpan.Automation.EntryPoint" />
      <DisplayName Value="exit" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.EntryPoint" />
      <InstanceUniqueId Value="EMPTY" />
      <LabelName Value="exit" />
      <MemberDetails Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="_param1" canRead="True" canWrite="True" type="System.Boolean" aliasName="result" shouldSerialize="False" visibility="AlwaysHidden" source="" blockTypeName="" />
          <OpenSpan.DynamicMembers.DynamicMethodInfo dynamicType="Method" name="GoToLabel" aliasName="GoToLabel" visibility="AlwaysHidden" source="" blockTypeName="" returnType="System.Void">
            <param name="_param1" aliasName="result" paramType="System.Boolean" isIn="True" isOut="False" position="0" />
            <param name="_param2" aliasName="msg" paramType="System.String" isIn="True" isOut="False" position="1" />
          </OpenSpan.DynamicMembers.DynamicMethodInfo>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="_param2" canRead="True" canWrite="True" type="System.String" aliasName="msg" shouldSerialize="False" visibility="AlwaysHidden" source="" blockTypeName="" />
        </Items>
      </Content>
    </OpenSpan.Automation.LabelHost>
    <OpenSpan.Automation.ExitPoint Name="exitPoint1" Id="ExitPoint-8D89A5C55B3554A">
      <ComponentName Value="Execute" />
      <DisplayName Value="Exit" />
      <EntryPoint Value="ComponentReference" Name="entryPoint1" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.EntryPoint" />
      <InstanceUniqueId Value="Automator-8D89A5C39600A9A\EntryPoint-8D89A5C42440183" />
      <MemberDetails Value="" />
      <Content Name="DynamicMembers">
        <Items>
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="Result" canRead="True" canWrite="True" type="System.Void" aliasName="Result" shouldSerialize="False" visibility="DefaultOn" source="" blockTypeName="" />
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="param1" canRead="True" canWrite="True" type="System.Boolean" aliasName="Result" shouldSerialize="False" visibility="AlwaysHidden" source="" blockTypeName="" />
          <OpenSpan.DynamicMembers.DynamicPropertyInfo dynamicType="Property" name="_param1" canRead="True" canWrite="True" type="System.String" aliasName="Msg" shouldSerialize="False" visibility="AlwaysHidden" source="" blockTypeName="" />
        </Items>
      </Content>
    </OpenSpan.Automation.ExitPoint>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties1" Id="ConnectableProperties-8D89A5D17960E12">
      <ComponentName Value="pg_ME0200" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D8965C03C8B014" />
      <MemberDetails Value=".IsCreated Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="IsCreated" />
            <MemberType Value="Property" />
            <TypeName Value="System.Boolean" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod1" Id="ConnectableMethod-8D89A5D30BDE399">
      <ComponentName Value="pg_ME0200" />
      <DisplayName Value="CloseWindow" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D8965C03C8B014" />
      <MemberDetails Value=".CloseWindow() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="CloseWindow" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod2" Id="ConnectableMethod-8D89A5D367D8487">
      <ComponentName Value="pg_ME0210" />
      <DisplayName Value="CloseWindow" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D89667CFCDCD42" />
      <MemberDetails Value=".CloseWindow() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="CloseWindow" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties2" Id="ConnectableProperties-8D89A5D39D78F58">
      <ComponentName Value="pg_ME0210" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D89667CFCDCD42" />
      <MemberDetails Value=".IsCreated Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="IsCreated" />
            <MemberType Value="Property" />
            <TypeName Value="System.Boolean" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties3" Id="ConnectableProperties-8D89A5D3E873511">
      <ComponentName Value="pg_ME0300" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D8977B7B56463E" />
      <MemberDetails Value=".IsCreated Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="IsCreated" />
            <MemberType Value="Property" />
            <TypeName Value="System.Boolean" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod3" Id="ConnectableMethod-8D89A5D42904172">
      <ComponentName Value="pg_ME0300" />
      <DisplayName Value="CloseWindow" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D8977B7B56463E" />
      <MemberDetails Value=".CloseWindow() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="CloseWindow" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod4" Id="ConnectableMethod-8D89A5D4A1581C9">
      <ComponentName Value="pg_RF0600" />
      <DisplayName Value="CloseWindow" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D896AFD851A4A7" />
      <MemberDetails Value=".CloseWindow() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="CloseWindow" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties4" Id="ConnectableProperties-8D89A5D4E628963">
      <ComponentName Value="pg_RF0600" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D896AFD851A4A7" />
      <MemberDetails Value=".IsCreated Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="IsCreated" />
            <MemberType Value="Property" />
            <TypeName Value="System.Boolean" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties5" Id="ConnectableProperties-8D89A5D54BA21B4">
      <ComponentName Value="pg_RM0100" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D896B1DDAAC2D8" />
      <MemberDetails Value=".IsCreated Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="IsCreated" />
            <MemberType Value="Property" />
            <TypeName Value="System.Boolean" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod5" Id="ConnectableMethod-8D89A5D59276C4B">
      <ComponentName Value="pg_RM0100" />
      <DisplayName Value="CloseWindow" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D896B1DDAAC2D8" />
      <MemberDetails Value=".CloseWindow() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="CloseWindow" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod6" Id="ConnectableMethod-8D89A5D63D428E0">
      <ComponentName Value="pg_BR0200" />
      <DisplayName Value="CloseWindow" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D8977D888962B2" />
      <MemberDetails Value=".CloseWindow() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="CloseWindow" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties6" Id="ConnectableProperties-8D89A5D67BA91DC">
      <ComponentName Value="pg_BR0200" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D8977D888962B2" />
      <MemberDetails Value=".IsCreated Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="IsCreated" />
            <MemberType Value="Property" />
            <TypeName Value="System.Boolean" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableProperties Name="connectableProperties7" Id="ConnectableProperties-8D89A5D6B891DA1">
      <ComponentName Value="pg_1095Info" />
      <DefaultValues Value="" />
      <DisplayName Value="Properties" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D896B2CCF6739C" />
      <MemberDetails Value=".IsCreated Property" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="IsCreated" />
            <MemberType Value="Property" />
            <TypeName Value="System.Boolean" />
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableProperties>
    <OpenSpan.Automation.ConnectableMethod Name="connectableMethod7" Id="ConnectableMethod-8D89A5D7071FF30">
      <ComponentName Value="pg_1095Info" />
      <DisplayName Value="CloseWindow" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Adapters.Web.Controls.WebPage" />
      <InstanceUniqueId Value="WebAdapter-8D892BE8D490EE1\WebPage-8D896B2CCF6739C" />
      <MemberDetails Value=".CloseWindow() Method" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="CloseWindow" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.ConnectableMethod>
    <OpenSpan.Automation.JumpHost Name="jumpHost1" Id="JumpHost-8D89A5F58EE740D">
      <ComponentName Value="labelHost1" />
      <DisplayName Value="GoToLabel" />
      <ExceptionsHandled Value="False" />
      <InstanceTypeName Value="OpenSpan.Automation.LabelHost" />
      <InstanceUniqueId Value="Automator-8D89A5C39600A9A\LabelHost-8D89A5C4F753C33" />
      <MemberDetails Value=" - exit" />
      <ParamsLength Value="0" />
      <SerializedParamsDefaultValues Value="" />
      <Content Name="MemberPrototypes">
        <Items>
          <OpenSpan.Automation.MemberPrototype>
            <MemberName Value="GoToLabel" />
            <MemberType Value="Method" />
            <TypeName Value="System.Void" />
            <Content Name="Signature">
              <OpenSpan.Automation.MethodSignature>
                <ReturnType Value="System.Void" />
                <Content Name="ParameterPrototype">
                  <Items>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="False" />
                      <CanWrite Value="True" />
                      <DefaultSet Value="True" />
                      <DefaultValue Value="True" />
                      <ParamName Value="_param1" />
                      <Position Value="0" />
                      <TypeName Value="System.Boolean" />
                    </OpenSpan.Automation.ParameterPrototype>
                    <OpenSpan.Automation.ParameterPrototype>
                      <CanRead Value="False" />
                      <CanWrite Value="True" />
                      <DefaultSet Value="True" />
                      <DefaultValue Value="" />
                      <ParamName Value="_param2" />
                      <Position Value="1" />
                      <TypeName Value="System.String" />
                    </OpenSpan.Automation.ParameterPrototype>
                  </Items>
                </Content>
              </OpenSpan.Automation.MethodSignature>
            </Content>
          </OpenSpan.Automation.MemberPrototype>
        </Items>
      </Content>
    </OpenSpan.Automation.JumpHost>
  </Component>
</OpenSpanDesignDocument>
<OpenSpanDesignDocument Version="19.1" Serializer="2.0" Culture="en-US">
	<ComponentInfo>
		<Type Value="OpenSpan.Automation.Automator" />
		<Assembly Value="OpenSpan.Automation" />
		<AssemblyReferences>
			<Assembly Value="mscorlib" />
			<Assembly Value="System" />
			<Assembly Value="OpenSpan" />
			<Assembly Value="OpenSpan.Automation" />
		</AssemblyReferences>
		<DynamicAssemblyReferences />
		<FileReferences />
	</ComponentInfo>
	<Component Version="1.0">
		<OpenSpan.Automation.Automator Name="SGE_P_IR0600_1095Info" Id="Automator-8D8983C31C79D5E">
			<AutomationDocument>
				<Name Value="" />
				<Size Value="5000, 5000" />
				<Objects />
				<Links />
				<Comments />
				<SubGraphs />
			</AutomationDocument>
			<IsStartStoppable Value="False" />
			<LogData Value="True" />
			<LogEvents Value="True" />
			<LogFile Value="" />
			<LogToFile Value="False" />
			<SuppressErrors Value="False" />
		</OpenSpan.Automation.Automator>
	</Component>
</OpenSpanDesignDocument>
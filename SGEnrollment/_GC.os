<OpenSpanDesignDocument Version="19.1.0.25" Serializer="2.0" Culture="en-US">
  <ComponentInfo>
    <Type Value="OpenSpan.Automation.GlobalContainer" />
    <Assembly Value="OpenSpan.Automation" />
    <AssemblyReferences>
      <Assembly Value="System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <Assembly Value="mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <Assembly Value="OpenSpan, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.ApplicationFramework, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Automation, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Controls, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Interactions.Controls, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Script, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    </AssemblyReferences>
    <DynamicAssemblyReferences />
    <FileReferences />
    <BuildReferences />
  </ComponentInfo>
  <Component Version="1.0">
    <OpenSpan.Automation.GlobalContainer Name="_GC" Id="GlobalContainer-8D892BF0C8B166B" />
    <OpenSpan.Interactions.Controls.RobotActivity Name="ract_SGEnrollment" Id="RobotActivity-8D892BEED5379D8">
      <ActivityName Value="None" />
    </OpenSpan.Interactions.Controls.RobotActivity>
    <OpenSpan.Controls.DiagnosticsLog Name="diagnosticsLog1" Id="DiagnosticsLog-8D892BEE14518FB" />
    <OpenSpan.Controls.MessageDialog Name="messageDialog1" Id="MessageDialog-8D892BEE149DDA8">
      <Caption Value="Information" />
    </OpenSpan.Controls.MessageDialog>
    <OpenSpan.ApplicationFramework.MessageManifest.MessageManifest Name="messageManifest" Id="MessageManifest-8D892BEFBB2FAED" />
    <OpenSpan.Controls.StringUtils Name="stringUtils" Id="StringUtils-8D892BEE14C402F" />
    <OpenSpan.Script.Custom.Script Name="script" Id="Script-8D892C7EE36F0CD" />
    <OpenSpan.Controls.Pause Name="pause" Id="Pause-8D892C7F19BAF9F" />
    <OpenSpan.ApplicationFramework.AssistedSignOn.AsoManager Name="asoManager" Id="AsoManager-8D892C7FC2BEFE0">
      <Enable Value="False" />
      <QueueActivity Value="False" />
    </OpenSpan.ApplicationFramework.AssistedSignOn.AsoManager>
  </Component>
</OpenSpanDesignDocument>
<OpenSpanDesignDocument Version="19.1.0.25" Serializer="2.0" Culture="en-US">
  <ComponentInfo>
    <Type Value="OpenSpan.Adapters.Web.WebAdapter" />
    <Assembly Value="OpenSpan.Adapters.Web" />
    <AssemblyReferences>
      <Assembly Value="mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <Assembly Value="System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <Assembly Value="OpenSpan, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Adapters, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Adapters.ActiveX, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Adapters.Web, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Adapters.Windows, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.Adapters.WinInet, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
      <Assembly Value="OpenSpan.ApplicationFramework, Version=19.1.0.0, Culture=neutral, PublicKeyToken=f5db91edc02d8c5e" />
    </AssemblyReferences>
    <DynamicAssemblyReferences />
    <FileReferences>
      <File Value="OpenSpan.WinInet.x32.dll" />
      <File Value="OpenSpan.WinInet.x64.dll" />
    </FileReferences>
    <BuildReferences>
      <File Value="DefaultWindowFactoryConfiguration.xml" />
      <File Value="openspan.ini" />
      <File Value="OpenSpan.Translators.InternetExplorer.x32.dll" />
      <File Value="OpenSpan.Translators.InternetExplorer.x64.dll" />
      <File Value="OpenSpan.WinInet.x32.dll" />
      <File Value="OpenSpan.WinInet.x64.dll" />
      <File Value="OpenSpan.x32.sys" />
      <File Value="OpenSpan.x64.sys" />
      <File Value="Pega.ActiveX.x32.dll" />
      <File Value="Pega.ActiveX.x64.dll" />
      <File Value="Pega.GlobalAllocator.x32.dll" />
      <File Value="Pega.GlobalAllocator.x64.dll" />
      <File Value="Pega.Native.Remoting.x32.dll" />
      <File Value="Pega.Native.Remoting.x64.dll" />
      <File Value="Pega.Scout.x32.dll" />
      <File Value="Pega.Scout.x64.dll" />
      <File Value="Pega.Security.x32.dll" />
      <File Value="Pega.Security.x64.dll" />
      <File Value="Pega.SharedMemory.x32.dll" />
      <File Value="Pega.SharedMemory.x64.dll" />
      <File Value="Pega.Sinon.x32.dll" />
      <File Value="Pega.Sinon.x64.dll" />
      <File Value="Pega.Utilities.x32.dll" />
      <File Value="Pega.Utilities.x64.dll" />
      <File Value="Pega.WER.x32.dll" />
      <File Value="Pega.WER.x64.dll" />
      <File Value="Pega.Windows.Broker.x32.dll" />
      <File Value="Pega.Windows.Broker.x64.dll" />
      <File Value="Pega.WinQueue.x32.dll" />
      <File Value="Pega.WinQueue.x64.dll" />
    </BuildReferences>
  </ComponentInfo>
  <Component Version="1.0">
    <OpenSpan.Adapters.Web.WebAdapter Name="Amisys" Id="WebAdapter-8D892BE8D490EE1">
      <HookChildProcesses Value="True" />
      <StartPage Value="http://hfhp-wcaa-tst.dstcorp.net:7128/amisys-web/Controller?view=jsp/Pportal.jsp" />
      <Content Name="Controls">
        <Items>
          <OpenSpan.Adapters.ActiveX.ActiveXFactory Name="ActiveXFactory" Id="ActiveXFactory-8D892C670C71F71">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <MatchingIndex Value="0" />
            <TargetTypeString Value="OpenSpan.Adapters.Windows.WindowsModule, OpenSpan.Adapters.Windows" />
            <UseKeys Value="True" />
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.Windows.MatchRules.ModuleNameMatchRule Name="moduleNameMatchRule1" Id="ModuleNameMatchRule-8D892C670D0A8FB">
                  <Text Value="Simple|True|(User Culture)|ole32.dll" />
                </OpenSpan.Adapters.Windows.MatchRules.ModuleNameMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.ActiveX.ActiveXFactory>
          <OpenSpan.Adapters.WinInet.WinInetFactory Name="WinInetFactory" Id="WinInetFactory-8D892C67107800E">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <MatchingIndex Value="0" />
            <TargetTypeString Value="OpenSpan.Adapters.Windows.WindowsModule, OpenSpan.Adapters.Windows" />
            <UseKeys Value="True" />
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.Windows.MatchRules.ModuleNameMatchRule Name="moduleNameMatchRule2" Id="ModuleNameMatchRule-8D892C6710EA731">
                  <Text Value="Simple|True|(User Culture)|wininet.dll" />
                </OpenSpan.Adapters.Windows.MatchRules.ModuleNameMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.WinInet.WinInetFactory>
          <OpenSpan.Adapters.Web.MicrosoftHTMLFactory Name="MicrosoftHTMLFactory" Id="MicrosoftHTMLFactory-8D892C672EB85B4">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <MatchingIndex Value="0" />
            <TargetTypeString Value="OpenSpan.Adapters.Windows.WindowsModule, OpenSpan.Adapters.Windows" />
            <UseKeys Value="True" />
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.Windows.MatchRules.ModuleNameMatchRule Name="moduleNameMatchRule3" Id="ModuleNameMatchRule-8D892C672F50F2A">
                  <Text Value="Simple|True|(User Culture)|mshtml.dll" />
                </OpenSpan.Adapters.Windows.MatchRules.ModuleNameMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.MicrosoftHTMLFactory>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="pg_Login" Id="WebPage-8D892C6801A3203">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="0" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmLogin" Id="Form-8D892C680156D38">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtj_username" Id="TextBox-8D892C6800BE3A7">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule1" Id="InputTypeMatchRule-8D892C6806DA5BA">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.InputNameMatchRule Name="inputNameMatchRule1" Id="InputNameMatchRule-8D892C680772F58">
                              <Text Value="Simple|True|(User Culture)|j_username" />
                            </OpenSpan.Adapters.Web.MatchRules.InputNameMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtj_password" Id="TextBox-8D892C685F02CC2">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="1" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputPasswordElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule2" Id="InputTypeMatchRule-8D892C6860CC97A">
                              <Type Value="Password" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.InputNameMatchRule Name="inputNameMatchRule2" Id="InputNameMatchRule-8D892C68613F0A7">
                              <Text Value="Simple|True|(User Culture)|j_password" />
                            </OpenSpan.Adapters.Web.MatchRules.InputNameMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.Button Name="btnSubmit" Id="Button-8D892C68B170C58">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="btnSubmit" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="3" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputSubmitElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule3" Id="InputTypeMatchRule-8D892C68B2A1F6B">
                              <Type Value="Submit" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule1" Id="ElementIdMatchRule-8D892C68B33A904">
                              <Text Value="Simple|True|(User Culture)|btnSubmit" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.Button>
                      <OpenSpan.Adapters.Web.Controls.Button Name="btnClear" Id="Button-8D892C68F8690E2">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="btnClear" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="4" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputResetElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule4" Id="InputTypeMatchRule-8D892C68F9C067B">
                              <Type Value="Reset" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule2" Id="ElementIdMatchRule-8D892C68FA59009">
                              <Text Value="Simple|True|(User Culture)|btnClear" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.Button>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.InnerTextMatchRule Name="innerTextMatchRule1" Id="InnerTextMatchRule-8D892C6CF570042">
                        <Text Value="Simple|True|(User Culture)|User Name:&#xD;&#xA; &#xD;&#xA;Password:" />
                      </OpenSpan.Adapters.Web.MatchRules.InnerTextMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.Web.MatchRules.DocumentTitleMatchRule Name="documentTitleMatchRule1" Id="DocumentTitleMatchRule-8D892C680477F76">
                  <Text Value="Simple|True|(User Culture)|AMISYS Advance Login" />
                </OpenSpan.Adapters.Web.MatchRules.DocumentTitleMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
          <OpenSpan.Adapters.Controls.Form Name="frmMessage" Id="Form-8D894CB04175166">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <MatchingIndex Value="14" />
            <TargetTypeString Value="OpenSpan.Adapters.Windows.Targets.Form, OpenSpan.Adapters.Windows" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Controls.Button Name="btnOK" Id="Button-8D894CB04102A21">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TargetTypeString Value="OpenSpan.Adapters.Windows.Targets.Button, OpenSpan.Adapters.Windows" />
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule Name="windowTextMatchRule2" Id="WindowTextMatchRule-8D894CB0465FF2B">
                        <Text Value="Simple|True|(User Culture)|OK" />
                      </OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule>
                      <OpenSpan.Adapters.Windows.MatchRules.ClassNameMatchRule Name="classNameMatchRule2" Id="ClassNameMatchRule-8D894CB046D25BA">
                        <ClassName Value="Button" />
                      </OpenSpan.Adapters.Windows.MatchRules.ClassNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Controls.Button>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule Name="windowTextMatchRule1" Id="WindowTextMatchRule-8D894CB04496259">
                  <Text Value="Simple|True|(User Culture)|Message from webpage" />
                </OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Controls.Form>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="pg_WelcomeHome" Id="WebPage-8D894CB70AF7ACC">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="2" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmsearchscreen" Id="Form-8D894CB70AAB60C">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="1" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtScreenId" Id="TextBox-8D894CB70A12CA7">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule5" Id="InputTypeMatchRule-8D894CB70F701B9">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.InputNameMatchRule Name="inputNameMatchRule3" Id="InputNameMatchRule-8D894CB70FE28D9">
                              <Text Value="Simple|True|(User Culture)|txtScreenId" />
                            </OpenSpan.Adapters.Web.MatchRules.InputNameMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.ImageButton Name="btnGO" Id="ImageButton-8D894CBBE54197F">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="GO" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="1" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputImageElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule6" Id="InputTypeMatchRule-8D894CBBE672C55">
                              <Type Value="Image" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule3" Id="ElementIdMatchRule-8D894CBBE6E5346">
                              <Text Value="Simple|True|(User Culture)|GO" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.ImageButton>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule Name="formNameMatchRule1" Id="FormNameMatchRule-8D894CB70E3EEFE">
                        <HtmlName Value="searchscreen" />
                      </OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.Web.MatchRules.DocumentTitleMatchRule Name="documentTitleMatchRule2" Id="DocumentTitleMatchRule-8D894CB70D33E5C">
                  <Text Value="Simple|True|(User Culture)|AMISYS Advance Welcome" />
                </OpenSpan.Adapters.Web.MatchRules.DocumentTitleMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="pg_ME0200" Id="WebPage-8D8965C03C8B014">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="4" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmME0200" Id="Form-8D8965C03C188E8">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.WebControl Name="lbl_ErrorMsg_ME0200" Id="WebControl-8D8965C03BCC413">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="ErrorMsg" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="DIV" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule4" Id="ElementIdMatchRule-8D8965C041036D1">
                              <Text Value="Simple|True|(User Culture)|ErrorMsg" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.WebControl>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_GROUP_NBR" Id="TextBox-8D8965C0919E83E">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_GROUP_NBR" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="1" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule7" Id="InputTypeMatchRule-8D8965C09283663">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule5" Id="ElementIdMatchRule-8D8965C0989F762">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_GROUP_NBR" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_NAME_X" Id="TextBox-8D8965C0E6C7E14">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_NAME_X" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="9" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule8" Id="InputTypeMatchRule-8D8965C0E7ACC36">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule6" Id="ElementIdMatchRule-8D8965C0ED303BC">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_NAME_X" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_CONTACT" Id="TextBox-8D8965C13406BCA">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_CONTACT" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="11" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule9" Id="InputTypeMatchRule-8D8965C13511C09">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule7" Id="ElementIdMatchRule-8D8965C13ABB5D8">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_CONTACT" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_CONTACT_ID" Id="TextBox-8D8965C17E0E231">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_CONTACT_ID" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="13" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule10" Id="InputTypeMatchRule-8D8965C17EF3062">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule8" Id="ElementIdMatchRule-8D8965C184E8ED2">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_CONTACT_ID" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_MKTREP" Id="TextBox-8D8965C1C6A055B">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_MKTREP" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="15" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule11" Id="InputTypeMatchRule-8D8965C1C7AB5D5">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule9" Id="ElementIdMatchRule-8D8965C1CD2ED53">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_MKTREP" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_ADDRESS1" Id="TextBox-8D8965C20873D20">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_ADDRESS1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="17" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule12" Id="InputTypeMatchRule-8D8965C209A4FFA">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule10" Id="ElementIdMatchRule-8D8965C20F4E9C9">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_ADDRESS1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_ADDRESS2" Id="TextBox-8D8965C24528685">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_ADDRESS2" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="19" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule13" Id="InputTypeMatchRule-8D8965C2463371B">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule11" Id="ElementIdMatchRule-8D8965C24BB6EAD">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_ADDRESS2" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_CITY" Id="TextBox-8D8965C28925F67">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_CITY" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="25" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule14" Id="InputTypeMatchRule-8D8965C28A31009">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule12" Id="ElementIdMatchRule-8D8965C28FDA9C4">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_CITY" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_STATE" Id="TextBox-8D8965C2FDF1ECF">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_STATE" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="27" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule15" Id="InputTypeMatchRule-8D8965C2FF23192">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule13" Id="ElementIdMatchRule-8D8965C304A6900">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_STATE" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_ZIP" Id="TextBox-8D8965C341DCD17">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_ZIP" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="23" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule16" Id="InputTypeMatchRule-8D8965C342E7DB8">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule14" Id="ElementIdMatchRule-8D8965C34891773">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_ZIP" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_COUNTRY" Id="TextBox-8D8965C388A28FC">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_COUNTRY" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="21" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule17" Id="InputTypeMatchRule-8D8965C389AD99D">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule15" Id="ElementIdMatchRule-8D8965C38F31107">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_COUNTRY" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_COUNTY" Id="TextBox-8D8965C3D126571">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_COUNTY" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="29" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule18" Id="InputTypeMatchRule-8D8965C3D257857">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule16" Id="ElementIdMatchRule-8D8965C3D827492">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_COUNTY" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_BUSINESS_UNIT" Id="TextBox-8D8965C4270C90B">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_BUSINESS_UNIT" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="57" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule19" Id="InputTypeMatchRule-8D8965C4288A0AE">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule17" Id="ElementIdMatchRule-8D8965C42E0D843">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_BUSINESS_UNIT" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_PROG_NBR" Id="TextBox-8D8965CB7215046">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_PROG_NBR" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="59" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule20" Id="InputTypeMatchRule-8D8965CB73927DF">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule18" Id="ElementIdMatchRule-8D8965CB7988643">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_PROG_NBR" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_CARRIER" Id="TextBox-8D8965CBCCF1608">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_CARRIER" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="61" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule21" Id="InputTypeMatchRule-8D8965CBCE6ED85">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule19" Id="ElementIdMatchRule-8D8965CBD3A6020">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_CARRIER" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_REGION" Id="TextBox-8D8965CC20AA25E">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_REGION" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="63" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule22" Id="InputTypeMatchRule-8D8965CC22279DC">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule20" Id="ElementIdMatchRule-8D8965CC275EC6B">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_REGION" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_GROUP_CAT" Id="TextBox-8D8965CC7B8CD11">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_GROUP_CAT" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="65" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule23" Id="InputTypeMatchRule-8D8965CC7D0A48E">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule21" Id="ElementIdMatchRule-8D8965CC828DBF4">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_GROUP_CAT" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_SPAN_YMDEFF" Id="TextBox-8D8965CD6391E12">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_SPAN_YMDEFF" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="77" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule24" Id="InputTypeMatchRule-8D8965CD65CE144">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule22" Id="ElementIdMatchRule-8D8965CD6B05412">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_SPAN_YMDEFF" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_SPAN_YMDEND" Id="TextBox-8D8965CDB479900">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_SPAN_YMDEND" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="79" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule25" Id="InputTypeMatchRule-8D8965CDB6B5C49">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule23" Id="ElementIdMatchRule-8D8965CDBC85871">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_SPAN_YMDEND" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_SPAN_REASON" Id="TextBox-8D8965CDFD71B34">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_SPAN_REASON" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="81" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule26" Id="InputTypeMatchRule-8D8965CDFF3B760">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule24" Id="ElementIdMatchRule-8D8965CE050B38C">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_SPAN_REASON" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_GROUP_ID" Id="TextBox-8D8966314CEBEA0">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_GROUP_ID" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="73" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule28" Id="InputTypeMatchRule-8D89663150E12A6">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule28" Id="ElementIdMatchRule-8D8966315E0F6C0">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_GROUP_ID" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTEMP_EFF" Id="TextBox-8D89666A41C038A">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTEMP_EFF" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="97" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule31" Id="InputTypeMatchRule-8D89666A4422957">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule30" Id="ElementIdMatchRule-8D89666A4959C34">
                              <Text Value="Simple|True|(User Culture)|txtTEMP_EFF" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTEMP_END" Id="TextBox-8D89666A9CC9EDB">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTEMP_END" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="99" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule32" Id="InputTypeMatchRule-8D89666A9EB9DA2">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule31" Id="ElementIdMatchRule-8D89666AA4AFC0E">
                              <Text Value="Simple|True|(User Culture)|txtTEMP_END" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_EIN" Id="TextBox-8D896AD09CFF3C0">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_EIN" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="3" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule36" Id="InputTypeMatchRule-8D896AD0A0467E2">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule36" Id="ElementIdMatchRule-8D896AD0B012273">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_EIN" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_PHONE1INFO" Id="TextBox-8D896AD57448457">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_PHONE1INFO" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="33" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule37" Id="InputTypeMatchRule-8D896AD575C5BCA">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule37" Id="ElementIdMatchRule-8D896AD57B49325">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_PHONE1INFO" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtSCREENPHONE" Id="TextBox-8D896AD5F0ECD9E">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtSCREENPHONE" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="35" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule38" Id="InputTypeMatchRule-8D896AD5F21E041">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule38" Id="ElementIdMatchRule-8D896AD5F77B57B">
                              <Text Value="Simple|True|(User Culture)|txtSCREENPHONE" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_YMDANNIV" Id="TextBox-8D896AE02E34104">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_YMDANNIV" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="37" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule39" Id="InputTypeMatchRule-8D896AE02FB1874">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule39" Id="ElementIdMatchRule-8D896AE035814E9">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_YMDANNIV" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_YMDREVIEW" Id="TextBox-8D896AE0758CD5C">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_YMDREVIEW" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="41" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule40" Id="InputTypeMatchRule-8D896AE076E42A3">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule40" Id="ElementIdMatchRule-8D896AE07C417D4">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_YMDREVIEW" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTEMP_ANNIV" Id="TextBox-8D896AE0C4EAFDD">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTEMP_ANNIV" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="39" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule41" Id="InputTypeMatchRule-8D896AE0C64251B">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule41" Id="ElementIdMatchRule-8D896AE0CB9FA31">
                              <Text Value="Simple|True|(User Culture)|txtTEMP_ANNIV" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTEMP_REVIEW" Id="TextBox-8D896AE10BAA843">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTEMP_REVIEW" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="43" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule42" Id="InputTypeMatchRule-8D896AE10D01D4F">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule42" Id="ElementIdMatchRule-8D896AE112854C5">
                              <Text Value="Simple|True|(User Culture)|txtTEMP_REVIEW" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_SIC" Id="TextBox-8D896AE31A13F4D">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_SIC" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="71" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule43" Id="InputTypeMatchRule-8D896AE31B916C4">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule43" Id="ElementIdMatchRule-8D896AE322B886D">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_SIC" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_BILL_METHOD" Id="TextBox-8D896AE85FB2CFD">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_BILL_METHOD" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="67" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule44" Id="InputTypeMatchRule-8D896AE8615670F">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule44" Id="ElementIdMatchRule-8D896AE866D9E85">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_BILL_METHOD" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_BILL_FORMAT" Id="TextBox-8D896AE8A8FC174">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_BILL_FORMAT" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="69" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule45" Id="InputTypeMatchRule-8D896AE8AA9FB5F">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule45" Id="ElementIdMatchRule-8D896AE8AFD6E29">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_BILL_FORMAT" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule Name="formNameMatchRule2" Id="FormNameMatchRule-8D8965C0401E8B7">
                        <HtmlName Value="ME0200" />
                      </OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmsearchscreen_ME0200" Id="Form-8D8966579FC7203">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="1" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtScreenId1" Id="TextBox-8D8966579F0863E">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule29" Id="InputTypeMatchRule-8D896657A2297F3">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.InputNameMatchRule Name="inputNameMatchRule4" Id="InputNameMatchRule-8D896657A2C2160">
                              <Text Value="Simple|True|(User Culture)|txtScreenId" />
                            </OpenSpan.Adapters.Web.MatchRules.InputNameMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.ImageButton Name="btnGO1" Id="ImageButton-8D896657FC408FD">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="GO" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="1" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputImageElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule30" Id="InputTypeMatchRule-8D896657FD71C0B">
                              <Type Value="Image" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule27" Id="ElementIdMatchRule-8D896657FDE4310">
                              <Text Value="Simple|True|(User Culture)|GO" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.ImageButton>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule Name="formNameMatchRule3" Id="FormNameMatchRule-8D896657A11E746">
                        <HtmlName Value="searchscreen" />
                      </OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
                <OpenSpan.Adapters.Web.Controls.Link Name="lnkAddress" Id="Link-8D896AF8AD93D5F">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ElementId Value="2ADDRESS" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="26" />
                  <TagName Value="A" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlAnchorElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="True" />
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule46" Id="ElementIdMatchRule-8D896AF8B01C574">
                        <Text Value="Simple|True|(User Culture)|2ADDRESS" />
                      </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Link>
                <OpenSpan.Adapters.Web.Controls.Link Name="lnkRemarks" Id="Link-8D896B0F3B4026A">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ElementId Value="10RR" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="34" />
                  <TagName Value="A" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlAnchorElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="True" />
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule54" Id="ElementIdMatchRule-8D896B0F3D56399">
                        <Text Value="Simple|True|(User Culture)|10RR" />
                      </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Link>
                <OpenSpan.Adapters.Web.Controls.Link Name="lnk1095_Information" Id="Link-8D896B29E629E59">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ElementId Value="12INFO" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="36" />
                  <TagName Value="A" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlAnchorElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="True" />
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule63" Id="ElementIdMatchRule-8D896B29E83FF55">
                        <Text Value="Simple|True|(User Culture)|12INFO" />
                      </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Link>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule Name="controlChildrenMatchRule1" Id="ControlChildrenMatchRule-8D8965D3604C61A">
                  <Content Name="Children">
                    <Capacity Value="4" />
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.Form Value="ComponentReference" Name="frmME0200" />
                    </Items>
                  </Content>
                </OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="pg_ME0210" Id="WebPage-8D89667CFCDCD42">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="6" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmME0210" Id="Form-8D89667CFC90899">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_CYCLE" Id="TextBox-8D89667CFC1E1B3">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_CYCLE" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="13" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule33" Id="InputTypeMatchRule-8D89667D01EDDBB">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule32" Id="ElementIdMatchRule-8D89667D0B9D774">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_CYCLE" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtGROUP_M_CYCLE_DAY" Id="TextBox-8D89667D44A1D8B">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtGROUP_M_CYCLE_DAY" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="15" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule34" Id="InputTypeMatchRule-8D89667D4586BC4">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule33" Id="ElementIdMatchRule-8D89667D491A477">
                              <Text Value="Simple|True|(User Culture)|txtGROUP_M_CYCLE_DAY" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtD_CYCLE_MONTHS" Id="TextBox-8D89667D8F0CA30">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtD_CYCLE_MONTHS" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="17" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule35" Id="InputTypeMatchRule-8D89667D8FF183A">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule34" Id="ElementIdMatchRule-8D89667D93850F4">
                              <Text Value="Simple|True|(User Culture)|txtD_CYCLE_MONTHS" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.WebControl Name="lbl_ErrorMsg_ME0210" Id="WebControl-8D89668B02A30F9">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="ErrorMsg" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="DIV" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="False" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule35" Id="ElementIdMatchRule-8D89668B0387F07">
                              <Text Value="Simple|True|(User Culture)|ErrorMsg" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.WebControl>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule Name="formNameMatchRule4" Id="FormNameMatchRule-8D89667D004A3A0">
                        <HtmlName Value="ME0210" />
                      </OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule Name="controlChildrenMatchRule3" Id="ControlChildrenMatchRule-8D8966801140FF0">
                  <Content Name="Children">
                    <Capacity Value="4" />
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.Form Value="ComponentReference" Name="frmME0210" />
                    </Items>
                  </Content>
                </OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="pg_RF0600" Id="WebPage-8D896AFD851A4A7">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="6" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmRF0600" Id="Form-8D896AFD84A7D7A">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtSCREENPHONE1" Id="TextBox-8D896AFD8435685">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtSCREENPHONE" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="49" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule46" Id="InputTypeMatchRule-8D896AFD8CD9F79">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule47" Id="ElementIdMatchRule-8D896AFD9AB5B76">
                              <Text Value="Simple|True|(User Culture)|txtSCREENPHONE" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtSCREENPHONE2" Id="TextBox-8D896AFDC65BED4">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtSCREENPHONE2" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="55" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule47" Id="InputTypeMatchRule-8D896AFDC7FF888">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule48" Id="ElementIdMatchRule-8D896AFDCD108E1">
                              <Text Value="Simple|True|(User Culture)|txtSCREENPHONE2" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_EMAIL" Id="TextBox-8D896AFE26DC6A0">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_EMAIL" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="71" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule48" Id="InputTypeMatchRule-8D896AFE288008B">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule49" Id="ElementIdMatchRule-8D896AFE2D6AE94">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_EMAIL" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtSCREENPHONE3" Id="TextBox-8D896AFE655008F">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtSCREENPHONE3" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="67" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule49" Id="InputTypeMatchRule-8D896AFE66CD7FB">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule50" Id="ElementIdMatchRule-8D896AFE6BB861F">
                              <Text Value="Simple|True|(User Culture)|txtSCREENPHONE3" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_PHONE2INFO" Id="TextBox-8D896AFEB78ECDC">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_PHONE2INFO" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="51" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule50" Id="InputTypeMatchRule-8D896AFEB90C4A6">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule51" Id="ElementIdMatchRule-8D896AFEBE43760">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_PHONE2INFO" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS_PHONE3INFO" Id="TextBox-8D896AFEFF6A7B7">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS_PHONE3INFO" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="63" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule51" Id="InputTypeMatchRule-8D896AFF010E17A">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule52" Id="ElementIdMatchRule-8D896AFF061F1E3">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS_PHONE3INFO" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.WebControl Name="lbl_ErrorMsg_RF0600" Id="WebControl-8D896B03929786C">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="ErrorMsg" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="DIV" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule53" Id="ElementIdMatchRule-8D896B0393C8B81">
                              <Text Value="Simple|True|(User Culture)|ErrorMsg" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.WebControl>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule Name="formNameMatchRule5" Id="FormNameMatchRule-8D896AFD8920462">
                        <HtmlName Value="RF0600" />
                      </OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule Name="controlChildrenMatchRule4" Id="ControlChildrenMatchRule-8D896B016E4B6C0">
                  <Content Name="Children">
                    <Capacity Value="4" />
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.Form Value="ComponentReference" Name="frmRF0600" />
                    </Items>
                  </Content>
                </OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="pg_RM0100" Id="WebPage-8D896B1DDAAC2D8">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="6" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmRM0100" Id="Form-8D896B1DDA5FE1B">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtT_RMKCAT" Id="TextBox-8D896B1DDA13977">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtT_RMKCAT" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="3" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule52" Id="InputTypeMatchRule-8D896B1DDF249D4">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule55" Id="ElementIdMatchRule-8D896B1DEE57B1F">
                              <Text Value="Simple|True|(User Culture)|txtT_RMKCAT" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDISP_NUM" Id="TextBox-8D896B1E5526AD6">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDISP_NUM" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="9" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule53" Id="InputTypeMatchRule-8D896B1E560B8EC">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule56" Id="ElementIdMatchRule-8D896B1E5B8F056">
                              <Text Value="Simple|True|(User Culture)|txtDISP_NUM" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtREMARK_SUMMARY_YMDEFF" Id="TextBox-8D896B1EBB477F8">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtREMARK_SUMMARY_YMDEFF" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="17" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule54" Id="InputTypeMatchRule-8D896B1EBC2C649">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule57" Id="ElementIdMatchRule-8D896B1EC189B3C">
                              <Text Value="Simple|True|(User Culture)|txtREMARK_SUMMARY_YMDEFF" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtREMARK_SUMMARY_YMDEND" Id="TextBox-8D896B1F0A86EA6">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtREMARK_SUMMARY_YMDEND" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="19" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule55" Id="InputTypeMatchRule-8D896B1F0B91F20">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule58" Id="ElementIdMatchRule-8D896B1F10C91DE">
                              <Text Value="Simple|True|(User Culture)|txtREMARK_SUMMARY_YMDEND" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtREMARK_SUMMARY_RMKTYPE" Id="TextBox-8D896B1F496D794">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtREMARK_SUMMARY_RMKTYPE" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="21" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule56" Id="InputTypeMatchRule-8D896B1F4A7880E">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule59" Id="ElementIdMatchRule-8D896B1F4F8989E">
                              <Text Value="Simple|True|(User Culture)|txtREMARK_SUMMARY_RMKTYPE" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtSEC_LEVEL" Id="TextBox-8D896B1F99C804A">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtSEC_LEVEL" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="23" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule57" Id="InputTypeMatchRule-8D896B1F9AF9324">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule60" Id="ElementIdMatchRule-8D896B1FA00A35A">
                              <Text Value="Simple|True|(User Culture)|txtSEC_LEVEL" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtREMARK_SUMMARY_REMARK" Id="TextBox-8D896B1FEDAEA73">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtREMARK_SUMMARY_REMARK" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="25" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule58" Id="InputTypeMatchRule-8D896B1FEEB9AED">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule61" Id="ElementIdMatchRule-8D896B1FF3F0DA7">
                              <Text Value="Simple|True|(User Culture)|txtREMARK_SUMMARY_REMARK" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.WebControl Name="lbl_ErrorMsg_RM0100" Id="WebControl-8D896B242B1888E">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="ErrorMsg" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="DIV" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule62" Id="ElementIdMatchRule-8D896B242C238C5">
                              <Text Value="Simple|True|(User Culture)|ErrorMsg" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.WebControl>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule Name="formNameMatchRule6" Id="FormNameMatchRule-8D896B1DDE19936">
                        <HtmlName Value="RM0100" />
                      </OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule Name="controlChildrenMatchRule5" Id="ControlChildrenMatchRule-8D896B22990B60E">
                  <Content Name="Children">
                    <Capacity Value="4" />
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.Form Value="ComponentReference" Name="frmRM0100" />
                    </Items>
                  </Content>
                </OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="pg_1095Info" Id="WebPage-8D896B2CCF6739C">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="6" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmIR0600" Id="Form-8D896B2CCEF4C83">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtEIN" Id="TextBox-8D896B2CCE82582">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtEIN" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="1" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule59" Id="InputTypeMatchRule-8D896B2CD3935B4">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule64" Id="ElementIdMatchRule-8D896B2CDAE0985">
                              <Text Value="Simple|True|(User Culture)|txtEIN" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTAXYEAR" Id="TextBox-8D896B2D16A9CE5">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTAXYEAR" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="3" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule60" Id="InputTypeMatchRule-8D896B2D178EAEF">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule65" Id="ElementIdMatchRule-8D896B2D1A17323">
                              <Text Value="Simple|True|(User Culture)|txtTAXYEAR" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtBUSINESSNAMECONTROL" Id="TextBox-8D896B2D5706D9D">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtBUSINESSNAMECONTROL" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="7" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule61" Id="InputTypeMatchRule-8D896B2D57C5956">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule66" Id="ElementIdMatchRule-8D896B2D5A9A647">
                              <Text Value="Simple|True|(User Culture)|txtBUSINESSNAMECONTROL" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtBUSINESSNAME" Id="TextBox-8D896B2D94643FE">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtBUSINESSNAME" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="9" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule62" Id="InputTypeMatchRule-8D896B2D956F455">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule67" Id="ElementIdMatchRule-8D896B2D981DEFD">
                              <Text Value="Simple|True|(User Culture)|txtBUSINESSNAME" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtBUSINESSNAME2" Id="TextBox-8D896B2DCC1CC63">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtBUSINESSNAME2" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="11" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule63" Id="InputTypeMatchRule-8D896B2DCD01A95">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule68" Id="ElementIdMatchRule-8D896B2DCFB0512">
                              <Text Value="Simple|True|(User Culture)|txtBUSINESSNAME2" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS" Id="TextBox-8D896B2E0B14114">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="13" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule64" Id="InputTypeMatchRule-8D896B2E0C1F157">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule69" Id="ElementIdMatchRule-8D896B2E0EF3E30">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtADDRESS2" Id="TextBox-8D896B2E405F490">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtADDRESS2" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="15" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule65" Id="InputTypeMatchRule-8D896B2E416A4EE">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule70" Id="ElementIdMatchRule-8D896B2E443F1F3">
                              <Text Value="Simple|True|(User Culture)|txtADDRESS2" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtCITY" Id="TextBox-8D896B2E7C31861">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtCITY" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="17" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule66" Id="InputTypeMatchRule-8D896B2E7D16693">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule71" Id="ElementIdMatchRule-8D896B2E7FC510C">
                              <Text Value="Simple|True|(User Culture)|txtCITY" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtSTATE" Id="TextBox-8D896B2EB8C9E33">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtSTATE" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="19" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule67" Id="InputTypeMatchRule-8D896B2EB9FB0FA">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule72" Id="ElementIdMatchRule-8D896B2EBCA9B63">
                              <Text Value="Simple|True|(User Culture)|txtSTATE" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtZIPCODE" Id="TextBox-8D896B2EF496CEE">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtZIPCODE" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="21" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule68" Id="InputTypeMatchRule-8D896B2EF5A1D19">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule73" Id="ElementIdMatchRule-8D896B2EF82A569">
                              <Text Value="Simple|True|(User Culture)|txtZIPCODE" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.WebControl Name="lbl_ErrorMsg_1095Info" Id="WebControl-8D896B2FEC67C6F">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="ErrorMsg" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="DIV" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule74" Id="ElementIdMatchRule-8D896B2FED4CA49">
                              <Text Value="Simple|True|(User Culture)|ErrorMsg" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.WebControl>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule Name="formNameMatchRule7" Id="FormNameMatchRule-8D896B2CD28851E">
                        <HtmlName Value="IR0600" />
                      </OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule Name="controlChildrenMatchRule6" Id="ControlChildrenMatchRule-8D896B3346ABB44">
                  <Content Name="Children">
                    <Capacity Value="4" />
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.Form Value="ComponentReference" Name="frmIR0600" />
                    </Items>
                  </Content>
                </OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="pg_ME0300" Id="WebPage-8D8977B7B56463E">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="4" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmME0300" Id="Form-8D8977B7B518189">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_DIVISION_NBR" Id="TextBox-8D8977B7B4A5A78">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_DIVISION_NBR" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="2" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule69" Id="InputTypeMatchRule-8D8977B7B9DCD2E">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule75" Id="ElementIdMatchRule-8D8977B7CE47123">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_DIVISION_NBR" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_EIN" Id="TextBox-8D8977B80F238F1">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_EIN" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="16" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule70" Id="InputTypeMatchRule-8D8977B81008723">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule76" Id="ElementIdMatchRule-8D8977B81709618">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_EIN" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_NAME_X" Id="TextBox-8D8977B84F74DBB">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_NAME_X" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="12" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule71" Id="InputTypeMatchRule-8D8977B85059BE5">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule77" Id="ElementIdMatchRule-8D8977B857348CD">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_NAME_X" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_PROG_NBR" Id="TextBox-8D8977B8F482F16">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_PROG_NBR" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="14" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule72" Id="InputTypeMatchRule-8D8977B8F58DFA4">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule78" Id="ElementIdMatchRule-8D8977B8FC8EECD">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_PROG_NBR" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_DIV_CAT" Id="TextBox-8D8977B9411F0A8">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_DIV_CAT" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="24" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule73" Id="InputTypeMatchRule-8D8977B9422A14D">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule79" Id="ElementIdMatchRule-8D8977B94904E1D">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_DIV_CAT" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_DIV_STATUS" Id="TextBox-8D8977B97221285">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_DIV_STATUS" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="30" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule74" Id="InputTypeMatchRule-8D8977B973787C3">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule80" Id="ElementIdMatchRule-8D8977B97A9F93C">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_DIV_STATUS" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_NEWHIRE" Id="TextBox-8D8977B9A9BFD13">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_NEWHIRE" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="38" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule75" Id="InputTypeMatchRule-8D8977B9AB1723E">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule81" Id="ElementIdMatchRule-8D8977B9B1CBCB2">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_NEWHIRE" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_AGEPOLICY" Id="TextBox-8D8977B9D9513C3">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_AGEPOLICY" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="42" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule76" Id="InputTypeMatchRule-8D8977B9DA826C1">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule82" Id="ElementIdMatchRule-8D8977B9E15D36E">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_AGEPOLICY" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_BILL_METHOD" Id="TextBox-8D8977BA0C23407">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_BILL_METHOD" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="46" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule77" Id="InputTypeMatchRule-8D8977BA0D7A978">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule83" Id="ElementIdMatchRule-8D8977BA147B872">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_BILL_METHOD" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_BILL_TO" Id="TextBox-8D8977BA53275B9">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_BILL_TO" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="54" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule78" Id="InputTypeMatchRule-8D8977BA547EADF">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule84" Id="ElementIdMatchRule-8D8977BA5B33514">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_BILL_TO" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_SPAN_YMDEFF" Id="TextBox-8D8977BA9ED1644">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_SPAN_YMDEFF" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="84" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule79" Id="InputTypeMatchRule-8D8977BAA074FFC">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule85" Id="ElementIdMatchRule-8D8977BAA74FCD8">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_SPAN_YMDEFF" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_SPAN_YMDEND" Id="TextBox-8D8977BACEB130F">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_SPAN_YMDEND" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="86" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule80" Id="InputTypeMatchRule-8D8977BAD0C7437">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule86" Id="ElementIdMatchRule-8D8977BAD755C1B">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_SPAN_YMDEND" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_SPAN_TIER" Id="TextBox-8D8977BB047A9FC">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_SPAN_TIER" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="88" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule81" Id="InputTypeMatchRule-8D8977BB064467B">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule87" Id="ElementIdMatchRule-8D8977BB0D45584">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_SPAN_TIER" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_SPAN_PREMKEY" Id="TextBox-8D8977BBA0CCA73">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_SPAN_PREMKEY" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="92" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule82" Id="InputTypeMatchRule-8D8977BBA308DB0">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule88" Id="ElementIdMatchRule-8D8977BBA9975B4">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_SPAN_PREMKEY" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_SPAN_BENEFIT_PKG" Id="TextBox-8D8977BBD8EFADD">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_SPAN_BENEFIT_PKG" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="96" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule83" Id="InputTypeMatchRule-8D8977BBDB78312">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule89" Id="ElementIdMatchRule-8D8977BBE1E08C1">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_SPAN_BENEFIT_PKG" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_SPAN_DIV_TYPE" Id="TextBox-8D8977BC23A0C70">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_SPAN_DIV_TYPE" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="104" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule84" Id="InputTypeMatchRule-8D8977BC2629485">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule90" Id="ElementIdMatchRule-8D8977BC2CB7C8C">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_SPAN_DIV_TYPE" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_SPAN_SPECKEY" Id="TextBox-8D8977BC82AE165">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_SPAN_SPECKEY" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="98" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule85" Id="InputTypeMatchRule-8D8977BC8510741">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule91" Id="ElementIdMatchRule-8D8977BC8B9EF32">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_SPAN_SPECKEY" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVISION_SPAN_REASON" Id="TextBox-8D8977BCDCB4F2F">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVISION_SPAN_REASON" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="110" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule86" Id="InputTypeMatchRule-8D8977BCDEA4DE7">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule92" Id="ElementIdMatchRule-8D8977BCE57FA88">
                              <Text Value="Simple|True|(User Culture)|txtDIVISION_SPAN_REASON" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.WebControl Name="lbl_ErrorMsg_ME0300" Id="WebControl-8D8977C0405C41A">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="ErrorMsg" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="DIV" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule93" Id="ElementIdMatchRule-8D8977C041674E3">
                              <Text Value="Simple|True|(User Culture)|ErrorMsg" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.WebControl>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule Name="formNameMatchRule8" Id="FormNameMatchRule-8D8977B7B8D1CB0">
                        <HtmlName Value="ME0300" />
                      </OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
                <OpenSpan.Adapters.Web.Controls.Link Name="lnkBroKer_" Id="Link-8D8977C95069EBA">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ElementId Value="2BR" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="41" />
                  <TagName Value="A" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlAnchorElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="True" />
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule94" Id="ElementIdMatchRule-8D8977C953D750C">
                        <Text Value="Simple|True|(User Culture)|2BR" />
                      </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Link>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule Name="controlChildrenMatchRule7" Id="ControlChildrenMatchRule-8D8977BE29416C1">
                  <Content Name="Children">
                    <Capacity Value="4" />
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.Form Value="ComponentReference" Name="frmME0300" />
                    </Items>
                  </Content>
                </OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
          <OpenSpan.Adapters.Controls.Form Name="frmWindows_Internet_Explorer" Id="Form-8D8977CA7714763">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <MatchingIndex Value="11" />
            <TargetTypeString Value="OpenSpan.Adapters.Windows.Targets.Form, OpenSpan.Adapters.Windows" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Controls.Button Name="btnOK1" Id="Button-8D8977CA76ED67C">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TargetTypeString Value="OpenSpan.Adapters.Windows.Targets.Button, OpenSpan.Adapters.Windows" />
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule Name="windowTextMatchRule4" Id="WindowTextMatchRule-8D8977CA794122F">
                        <Text Value="Simple|True|(User Culture)|OK" />
                      </OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule>
                      <OpenSpan.Adapters.Windows.MatchRules.ClassNameMatchRule Name="classNameMatchRule3" Id="ClassNameMatchRule-8D8977CA7987F1E">
                        <ClassName Value="Button" />
                      </OpenSpan.Adapters.Windows.MatchRules.ClassNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Controls.Button>
                <OpenSpan.Adapters.Controls.Button Name="btnCancel" Id="Button-8D8977CA9F09F01">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="1" />
                  <TargetTypeString Value="OpenSpan.Adapters.Windows.Targets.Button, OpenSpan.Adapters.Windows" />
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule Name="windowTextMatchRule5" Id="WindowTextMatchRule-8D8977CA9F77CD2">
                        <Text Value="Simple|True|(User Culture)|Cancel" />
                      </OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule>
                      <OpenSpan.Adapters.Windows.MatchRules.ClassNameMatchRule Name="classNameMatchRule4" Id="ClassNameMatchRule-8D8977CA9FC10C4">
                        <ClassName Value="Button" />
                      </OpenSpan.Adapters.Windows.MatchRules.ClassNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Controls.Button>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule Name="windowTextMatchRule3" Id="WindowTextMatchRule-8D8977CA77F0322">
                  <Text Value="Simple|True|(User Culture)|Windows Internet Explorer" />
                </OpenSpan.Adapters.Windows.MatchRules.WindowTextMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Controls.Form>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="pg_BR0200" Id="WebPage-8D8977D888962B2">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="4" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="Controls">
              <Items>
                <OpenSpan.Adapters.Web.Controls.Form Name="frmBR0200" Id="Form-8D8977D88849E21">
                  <DummyPropertyToDetectReplaceUndo Value="0" />
                  <ForwardObjectExplorerEvent Value="True" />
                  <MatchingIndex Value="0" />
                  <TagName Value="FORM" />
                  <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlFormElement, OpenSpan.Adapters.Web" />
                  <UseElementId Value="False" />
                  <Content Name="Controls">
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVNUM" Id="TextBox-8D8977D887D76E5">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVNUM" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="1" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule87" Id="InputTypeMatchRule-8D8977D88CE873E">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule95" Id="ElementIdMatchRule-8D8977D8A912635">
                              <Text Value="Simple|True|(User Culture)|txtDIVNUM" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtDIVNAME" Id="TextBox-8D8977D8DBB9520">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtDIVNAME" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="3" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule88" Id="InputTypeMatchRule-8D8977D8DC780CA">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule96" Id="ElementIdMatchRule-8D8977D8E5DB5C3">
                              <Text Value="Simple|True|(User Culture)|txtDIVNAME" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTEMP_EFF1" Id="TextBox-8D8977D93363AA8">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTEMP_EFF" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="9" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule89" Id="InputTypeMatchRule-8D8977D934488BE">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule97" Id="ElementIdMatchRule-8D8977D93D3966F">
                              <Text Value="Simple|True|(User Culture)|txtTEMP_EFF" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTEMP_END1" Id="TextBox-8D8977D96EC055F">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTEMP_END" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="11" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule90" Id="InputTypeMatchRule-8D8977D96FA5380">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule98" Id="ElementIdMatchRule-8D8977D97908871">
                              <Text Value="Simple|True|(User Culture)|txtTEMP_END" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTEMP_EFF2" Id="TextBox-8D8977D9C1575C6">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTEMP_EFF2" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="15" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule91" Id="InputTypeMatchRule-8D8977D9C23C3B9">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule99" Id="ElementIdMatchRule-8D8977D9CB2D1C0">
                              <Text Value="Simple|True|(User Culture)|txtTEMP_EFF2" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTEMP_END2" Id="TextBox-8D8977D9FF60E10">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTEMP_END2" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="17" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule92" Id="InputTypeMatchRule-8D8977DA006BE7A">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule100" Id="ElementIdMatchRule-8D8977DA09A9107">
                              <Text Value="Simple|True|(User Culture)|txtTEMP_END2" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.RadioButton Name="rb1" Id="RadioButton-8D8977E4590CD6E">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="optRecord___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="22" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputRadioElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule93" Id="InputTypeMatchRule-8D8977E45A642A9">
                              <Type Value="Radio" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule101" Id="ElementIdMatchRule-8D8977E468FEA5F">
                              <Text Value="Simple|True|(User Culture)|optRecord___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.RadioButton>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtBROKER_AFF_BROKER_NBR___1" Id="TextBox-8D8977E8D4F7A9A">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtBROKER_AFF_BROKER_NBR___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="23" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule94" Id="InputTypeMatchRule-8D8977E8D628DA4">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule102" Id="ElementIdMatchRule-8D8977E8DF66009">
                              <Text Value="Simple|True|(User Culture)|txtBROKER_AFF_BROKER_NBR___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtRIDERTYPE1___1" Id="TextBox-8D8977E94039D8A">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtRIDERTYPE1___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="25" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule95" Id="InputTypeMatchRule-8D8977E94144E23">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule103" Id="ElementIdMatchRule-8D8977E94A820A4">
                              <Text Value="Simple|True|(User Culture)|txtRIDERTYPE1___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtFEE_SCHEDA1___1" Id="TextBox-8D8977EBDE6068C">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtFEE_SCHEDA1___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="27" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule96" Id="InputTypeMatchRule-8D8977EBDF91966">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule104" Id="ElementIdMatchRule-8D8977EBE91B0B7">
                              <Text Value="Simple|True|(User Culture)|txtFEE_SCHEDA1___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtRIDERTYPE2___1" Id="TextBox-8D8977EC398B189">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtRIDERTYPE2___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="29" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule97" Id="InputTypeMatchRule-8D8977EC3ABC463">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule105" Id="ElementIdMatchRule-8D8977EC441F968">
                              <Text Value="Simple|True|(User Culture)|txtRIDERTYPE2___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtFEE_SCHEDA2___1" Id="TextBox-8D8977ECC9A8F44">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtFEE_SCHEDA2___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="31" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule98" Id="InputTypeMatchRule-8D8977ECCADA22E">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule106" Id="ElementIdMatchRule-8D8977ECD4174D6">
                              <Text Value="Simple|True|(User Culture)|txtFEE_SCHEDA2___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtRIDERTYPE3___1" Id="TextBox-8D8977ED1F07961">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtRIDERTYPE3___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="33" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule99" Id="InputTypeMatchRule-8D8977ED2038C57">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule107" Id="ElementIdMatchRule-8D8977ED2A0E83E">
                              <Text Value="Simple|True|(User Culture)|txtRIDERTYPE3___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtFEE_SCHEDA3___1" Id="TextBox-8D8977ED71328D6">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtFEE_SCHEDA3___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="35" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule100" Id="InputTypeMatchRule-8D8977ED7289E3C">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule108" Id="ElementIdMatchRule-8D8977ED7BC70CC">
                              <Text Value="Simple|True|(User Culture)|txtFEE_SCHEDA3___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtPERCENT1___1" Id="TextBox-8D8977EDD377219">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtPERCENT1___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="59" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule101" Id="InputTypeMatchRule-8D8977EDD4F49CB">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule109" Id="ElementIdMatchRule-8D8977EDDE0BA0C">
                              <Text Value="Simple|True|(User Culture)|txtPERCENT1___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtPERCENT2___1" Id="TextBox-8D8977EE2D7423E">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtPERCENT2___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="61" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule102" Id="InputTypeMatchRule-8D8977EE2EF19E9">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule110" Id="ElementIdMatchRule-8D8977EE3854EBA">
                              <Text Value="Simple|True|(User Culture)|txtPERCENT2___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtPERCENT3___1" Id="TextBox-8D8977EE836B294">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtPERCENT3___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="63" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule103" Id="InputTypeMatchRule-8D8977EE84E8A1F">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule111" Id="ElementIdMatchRule-8D8977EE8DFFA53">
                              <Text Value="Simple|True|(User Culture)|txtPERCENT3___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtRIDERTYPE4___1" Id="TextBox-8D8977EED6B55BD">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtRIDERTYPE4___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="37" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule104" Id="InputTypeMatchRule-8D8977EED7C062F">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule112" Id="ElementIdMatchRule-8D8977EEE0D767F">
                              <Text Value="Simple|True|(User Culture)|txtRIDERTYPE4___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtPERCENT4___1" Id="TextBox-8D8977EF2987F8C">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtPERCENT4___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="65" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule105" Id="InputTypeMatchRule-8D8977EF2B056F8">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule113" Id="ElementIdMatchRule-8D8977EF341C730">
                              <Text Value="Simple|True|(User Culture)|txtPERCENT4___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtFEE_SCHEDA4___1" Id="TextBox-8D8977EF79BA80F">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtFEE_SCHEDA4___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="39" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule106" Id="InputTypeMatchRule-8D8977EF7B37F87">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule114" Id="ElementIdMatchRule-8D8977EF844EFA7">
                              <Text Value="Simple|True|(User Culture)|txtFEE_SCHEDA4___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtTEMP_REASON" Id="TextBox-8D8977F0D5ABEBC">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtTEMP_REASON" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="357" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule107" Id="InputTypeMatchRule-8D8977F0DA96CC1">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule115" Id="ElementIdMatchRule-8D8977F0E3D3F56">
                              <Text Value="Simple|True|(User Culture)|txtTEMP_REASON" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtRIDERTYPE5___1" Id="TextBox-8D8977F143B7221">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtRIDERTYPE5___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="41" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule108" Id="InputTypeMatchRule-8D8977F1450E767">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule116" Id="ElementIdMatchRule-8D8977F14E257AB">
                              <Text Value="Simple|True|(User Culture)|txtRIDERTYPE5___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtPERCENT5___1" Id="TextBox-8D8977F18C3346D">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtPERCENT5___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="67" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule109" Id="InputTypeMatchRule-8D8977F18DB0C1F">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule117" Id="ElementIdMatchRule-8D8977F196A19E8">
                              <Text Value="Simple|True|(User Culture)|txtPERCENT5___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtFEE_SCHEDA5___1" Id="TextBox-8D8977F1E3FC7CD">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtFEE_SCHEDA5___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="43" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule110" Id="InputTypeMatchRule-8D8977F1E553D2F">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule118" Id="ElementIdMatchRule-8D8977F1EEB7210">
                              <Text Value="Simple|True|(User Culture)|txtFEE_SCHEDA5___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtRIDERTYPE6___1" Id="TextBox-8D8977F23917CFA">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtRIDERTYPE6___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="45" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule111" Id="InputTypeMatchRule-8D8977F23A6F23C">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule119" Id="ElementIdMatchRule-8D8977F24386269">
                              <Text Value="Simple|True|(User Culture)|txtRIDERTYPE6___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtFEE_SCHEDA6___1" Id="TextBox-8D8977F2864CA09">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtFEE_SCHEDA6___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="47" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule112" Id="InputTypeMatchRule-8D8977F28757A8F">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule120" Id="ElementIdMatchRule-8D8977F29094D44">
                              <Text Value="Simple|True|(User Culture)|txtFEE_SCHEDA6___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtPERCENT6___1" Id="TextBox-8D8977F2CD1FF5C">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtPERCENT6___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="69" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule113" Id="InputTypeMatchRule-8D8977F2CF5C2C8">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule121" Id="ElementIdMatchRule-8D8977F2D826E2C">
                              <Text Value="Simple|True|(User Culture)|txtPERCENT6___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtRIDERTYPE7___1" Id="TextBox-8D8977F31F1A93E">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtRIDERTYPE7___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="49" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule114" Id="InputTypeMatchRule-8D8977F320980F4">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule122" Id="ElementIdMatchRule-8D8977F329D5375">
                              <Text Value="Simple|True|(User Culture)|txtRIDERTYPE7___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtFEE_SCHEDA7___1" Id="TextBox-8D8977F3652FEC3">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtFEE_SCHEDA7___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="51" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule115" Id="InputTypeMatchRule-8D8977F36687409">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule123" Id="ElementIdMatchRule-8D8977F36FC46A6">
                              <Text Value="Simple|True|(User Culture)|txtFEE_SCHEDA7___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.TextBox Name="txtPERCENT7___1" Id="TextBox-8D8977F3B294498">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="txtPERCENT7___1" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="71" />
                        <TagName Value="INPUT" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlInputTextElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule Name="inputTypeMatchRule116" Id="InputTypeMatchRule-8D8977F3B45E0B4">
                              <Type Value="Text" />
                            </OpenSpan.Adapters.Web.MatchRules.InputTypeMatchRule>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule124" Id="ElementIdMatchRule-8D8977F3BDE781D">
                              <Text Value="Simple|True|(User Culture)|txtPERCENT7___1" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.TextBox>
                      <OpenSpan.Adapters.Web.Controls.WebControl Name="lbl_ErrorMsg_BR0200" Id="WebControl-8D8977FA3323C59">
                        <DummyPropertyToDetectReplaceUndo Value="0" />
                        <ElementId Value="ErrorMsg" />
                        <ForwardObjectExplorerEvent Value="True" />
                        <MatchingIndex Value="0" />
                        <TagName Value="DIV" />
                        <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlElement, OpenSpan.Adapters.Web" />
                        <UseElementId Value="True" />
                        <Content Name="MatchRules">
                          <Items>
                            <OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule Name="elementIdMatchRule125" Id="ElementIdMatchRule-8D8977FA3408A63">
                              <Text Value="Simple|True|(User Culture)|ErrorMsg" />
                            </OpenSpan.Adapters.Web.MatchRules.ElementIdMatchRule>
                          </Items>
                        </Content>
                      </OpenSpan.Adapters.Web.Controls.WebControl>
                    </Items>
                  </Content>
                  <Content Name="MatchRules">
                    <Items>
                      <OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule Name="formNameMatchRule9" Id="FormNameMatchRule-8D8977D88BB7478">
                        <HtmlName Value="BR0200" />
                      </OpenSpan.Adapters.Web.MatchRules.FormNameMatchRule>
                    </Items>
                  </Content>
                </OpenSpan.Adapters.Web.Controls.Form>
              </Items>
            </Content>
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule Name="controlChildrenMatchRule8" Id="ControlChildrenMatchRule-8D8977E54E6F7E8">
                  <Content Name="Children">
                    <Capacity Value="4" />
                    <Items>
                      <OpenSpan.Adapters.Web.Controls.Form Value="ComponentReference" Name="frmBR0200" />
                    </Items>
                  </Content>
                </OpenSpan.Adapters.MatchRules.ControlChildrenMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
          <OpenSpan.Adapters.Web.Controls.WebPage Name="AMISYS_Advance_Welcome_Subtraction_" Id="WebPage-8D8981CA8630063">
            <DummyPropertyToDetectReplaceUndo Value="0" />
            <ForwardObjectExplorerEvent Value="True" />
            <IsGlobal Value="True" />
            <MatchingIndex Value="5" />
            <TargetTypeString Value="OpenSpan.Adapters.Web.HtmlDocument, OpenSpan.Adapters.Web" />
            <Content Name="MatchRules">
              <Items>
                <OpenSpan.Adapters.Web.MatchRules.DocumentUrlMatchRule Name="documentUrlMatchRule2" Id="DocumentUrlMatchRule-8D8981CA881FF12">
                  <Fragment Value="Simple|True|(User Culture)|" />
                  <Host Value="Simple|True|(User Culture)|hfhp-wcaa-tst.dstcorp.net" />
                  <Path Value="Simple|True|(User Culture)|/amisys-web/Controller" />
                  <Port Value="7128" />
                  <Query Value="Simple|True|(User Culture)|" />
                  <Scheme Value="Simple|True|(User Culture)|http" />
                </OpenSpan.Adapters.Web.MatchRules.DocumentUrlMatchRule>
                <OpenSpan.Adapters.Web.MatchRules.DocumentTitleMatchRule Name="documentTitleMatchRule3" Id="DocumentTitleMatchRule-8D8981CA8892663">
                  <Text Value="Simple|True|(User Culture)|AMISYS Advance Welcome - Rel 6.6.3.5 - Acct HEALTH FIRST HEALTH PLANS" />
                </OpenSpan.Adapters.Web.MatchRules.DocumentTitleMatchRule>
              </Items>
            </Content>
          </OpenSpan.Adapters.Web.Controls.WebPage>
        </Items>
      </Content>
      <Content Name="Credentials">
        <Items>
          <OpenSpan.ApplicationFramework.AssistedSignOn.Credential>
            <ApplicationKey Value="Amisys" />
            <LoginControl Value="WebAdapter-8D892BE8D490EE1\Button-8D892C68B170C58" />
            <PasswordControl Value="WebAdapter-8D892BE8D490EE1\TextBox-8D892C685F02CC2" />
            <UserNameControl Value="WebAdapter-8D892BE8D490EE1\TextBox-8D892C6800BE3A7" />
          </OpenSpan.ApplicationFramework.AssistedSignOn.Credential>
        </Items>
      </Content>
    </OpenSpan.Adapters.Web.WebAdapter>
  </Component>
</OpenSpanDesignDocument>